__author__ = 'Joonas'

def voiko_poistaa(rivi):
    if rivi[-20:] == "<Media jätetty pois>":
        return True
    loyt = False
    paikka = 0
    for i in range(15, len(rivi)):
        if rivi[i] == ":":
            paikka = i
            loyt = True
            break
    if not loyt:
        return True
    for i in range(paikka+2, len(rivi)):
        if rivi[i] != "?":
            return False
    return True

def jatkuuko_viesti(rivi):
    tiedot = rivi.split(".")
    if len(tiedot) < 4:
        return True
    if not (tiedot[0].isdigit() and tiedot[1].isdigit() and \
            tiedot[2][:3].isdigit() and tiedot[2][-1].isdigit()\
            and tiedot[3][0].isdigit()):
        return True
    return False

def palauta_tallennusmuoto(rivi):
    palautus = "\n"
    luku = ""
    jatka = 0
    for i in range(len(rivi)):
        if rivi[i].isdigit():
            luku += rivi[i]
        else:
            if len(luku) == 2 or len(luku) == 4:
                palautus += luku
                luku = ""
            elif len(luku) == 1:
                palautus += "0" + luku
                luku = ""
        if len(palautus) == 13:
            palautus += " " + rivi[i+3:]
            break
    return palautus






def main():
    nimi = "Pösteridata2017b.txt"
    teksti = ""
    tiedosto = open(nimi, "r")
    for rivi in tiedosto:
        rivi = rivi.strip()
        if jatkuuko_viesti(rivi):
            teksti += " " + rivi
        elif not voiko_poistaa(rivi):
            teksti += palauta_tallennusmuoto(rivi)
    tiedosto.close()
    tiedosto = open(nimi, "w")
    tiedosto.write(teksti)
    tiedosto.close()

main()