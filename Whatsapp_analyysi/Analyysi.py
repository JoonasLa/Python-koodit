__author__ = 'Joonas'

import codecs
import time

class Viesti:
    def __init__(self, aika, nimi, teksti):
        self.__aika = aika
        self.__nimi = nimi
        self.__teksti = teksti

    def palauta_kk(self):
        return self.__aika.palauta_kk()

    def palauta_pv(self):
        return self.__aika.palauta_pvm()

    def palauta_vvvv(self):
        return self.__aika.palauta_vvvv()

    def palauta_hh(self):
        return self.__aika.palauta_hh()

    def palauta_min(self):
        return self.__aika.palauta_min()

    def palauta_nimi(self):
        return(self.__nimi)

    def palauta_teksti(self):
        return(self.__teksti)

    def palauta_aika(self):
        return self.__aika

class Aika:
    def __init__(self, pvm, kk, vvvv, hh, min):
        self.__pvm = pvm
        self.__kk = kk
        self.__vvvv = vvvv
        self.__hh = hh
        self.__min = min

    def palauta_pvm(self):
        return self.__pvm

    def palauta_kk(self):
        return self.__kk

    def palauta_vvvv(self):
        return self.__vvvv

    def palauta_hh(self):
        return self.__hh

    def palauta_min(self):
        return self.__min

    def __str__(self):
        teksti = str(self.__pvm)+"."+str(self.__kk)+"."+str(self.__vvvv)+" klo "
        teksti += str(self.__hh).zfill(2)+"."+str(self.__min).zfill(2)
        return teksti

    def selvita_aiempi(self, b):
        if self.__vvvv == b.__vvvv:
            if self.__kk == b.__kk:
                if self.__pvm == b.__pvm:
                    if self.__hh == b.__hh:
                        if self.__min <= b.__min:
                            eka = self
                            toka = b
                        else:
                            eka = b
                            toka = self
                    elif self.__hh < b.__hh:
                        eka = self
                        toka = b
                    else:
                        eka = b
                        toka = self
                elif self.__pvm < b.__pvm:
                    eka = self
                    toka = b
                else:
                    eka = b
                    toka = self
            elif self.__kk < b.__kk:
                eka = self
                toka = b
            else:
                eka = b
                toka = self
        elif self.__vvvv < b.__vvvv:
            eka = self
            toka = b
        else:
            toka = self
            eka = b
        return eka, toka

    def vali(self, b):
        # Palauttaa kahden ajan välisen ajan minuutteina
        kuukaudet = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        palautus = self.selvita_aiempi(b)
        eka = palautus[0]
        toka = palautus[1]
        vi = eka.__vvvv
        ki = eka.__kk
        paivasumma = 0
        while vi < toka.__vvvv or ki < toka.__kk:
            paivasumma += kuukaudet[ki-1]
            if ki == 2 and vi%4 == 0:
                paivasumma += 1
            ki += 1
            if ki == 13:
                ki = 1
                vi += 1
        paivasumma += toka.__pvm - eka.__pvm
        minuutit = paivasumma*24*60
        minuutit += (toka.__hh - eka.__hh)*60
        minuutit += toka.__min - eka.__min
        return minuutit

def tiedot_rivista(rivi):
    pvm = int(rivi[:2])
    kk = int(rivi[2:4])
    vvvv = int(rivi[4: 8])
    hh = int(rivi[8:10])
    min = int(rivi[10:12])
    aika = Aika(pvm, kk, vvvv, hh, min)
    i = 13
    nimi = ""
    while rivi[i] != ":":
        nimi += rivi[i]
        i += 1
    teksti = rivi[i+2:]
    return aika, nimi, teksti

def muodosta_viestit(tiedost, alpvm, lopvm):
    viestit = []
    tiedosto = codecs.open(tiedost, "r")
    for rivi in tiedosto:
        rivi = rivi.rstrip()
        tiedot = tiedot_rivista(rivi)
        if tiedot[0].selvita_aiempi(alpvm)[0] == alpvm and\
            tiedot[0].selvita_aiempi(lopvm)[1] == lopvm:
            viestit.append(Viesti(tiedot[0], tiedot[1], tiedot[2]))
    return(viestit)

def laske_nimikohtainen(viestit, alpvm, lopvm):
    nimet = {}
    vali = alpvm.vali(lopvm)/(60*24)
    for viesti in viestit:
        nimi = viesti.palauta_nimi()
        if nimi in nimet:
            nimet[nimi] += 1
        else:
            nimet[nimi] = 0
    for henk in nimet:
        print("Henkilö", henk, "on lähettänyt", nimet[henk], "viestiä, eli keskimäärin",\
              round(nimet[henk]/vali, 2), "viestiä päivässä")

def laske_kuukausikohtainen(viestit):
    alku = 12*viestit[0].palauta_vvvv() + viestit[0].palauta_kk()
    loppu = 12*viestit[-1].palauta_vvvv() + viestit[-1].palauta_kk()
    i = 0
    summa = 0
    for luku in range(alku - 1, loppu):
        kuukausi = luku % 12 + 1
        vuosi = luku // 12
        while True:
            if i < len(viestit) and viestit[i].palauta_kk() == kuukausi and \
                viestit[i].palauta_vvvv() == vuosi:
                summa += 1
                i += 1
            else:
                print(str(kuukausi)+"/"+str(vuosi), summa)
                summa = 0
                break

def laske_paivakohtainen(viestit):
    paivat = {}
    for viesti in viestit:
        luku = viesti.palauta_vvvv()*372+(viesti.palauta_kk()-1)*31+\
        viesti.palauta_pv()-1
        if luku in paivat:
            paivat[luku] += 1
        else:
            paivat[luku] = 1
    alku = viestit[0].palauta_vvvv()*372+(viestit[0].palauta_kk()-1)*31+\
        viestit[0].palauta_pv()-1
    loppu = viestit[-1].palauta_vvvv()*372+(viestit[-1].palauta_kk()-1)*31+\
        viestit[-1].palauta_pv()-1
    for luku in range(alku, loppu+1):
        vuosi = luku // 372
        kuukausi = (luku - vuosi * 372)// 31  + 1
        paiva = luku % 31 + 1
        if luku in paivat:
            print(str(paiva)+"."+str(kuukausi)+"."+str(vuosi), paivat[luku])
        else:
            print(str(paiva)+"."+str(kuukausi)+"."+str(vuosi), 0)

def laske_tuntikohtainen(viestit):
    tunnit = [0]*24
    for viesti in viestit:
        tunti = viesti.palauta_hh()
        tunnit[tunti] += 1
    for i in range(24):
        print(i, tunnit[i])

def laske_minuuttikohtainen(viestit):
    minuutit = [0]*60
    for viesti in viestit:
        minuutti = viesti.palauta_min()
        minuutit[minuutti] += 1
    for i in range(60):
        print(i, minuutit[i])

def etsi_pisin_viesti(viestit):
    pituus = 0
    kokoelma = []
    for viesti in viestit:
        pituusi = len(viesti.palauta_teksti())
        if pituusi > pituus:
            kokoelma = [viesti]
            pituus = pituusi
        elif pituusi == pituus:
            kokoelma.append(viesti)
    print("Pisin viesti:")
    for viesti in kokoelma:
        pvm = str(viesti.palauta_pv()) + "." + str(viesti.palauta_kk()) + "." \
             + str(viesti.palauta_vvvv())
        print(pvm, viesti.palauta_nimi()+":", viesti.palauta_teksti())
        print("Pituus:", len(viesti.palauta_teksti()), "merkkiä")

def onko_rivinvaihtoja(viestit):
    for viesti in viestit:
        if "\n" in viesti.palauta_teksti():
            print(viesti.palauta_teksti())

def viesteja_lahetettu(viestit):
    print("Yhteensä viestejä lähetetty", len(viestit), "kappaletta")

def laske_merkkijonofrekvenssi(viestit, mjono):
    henk = {}
    for viesti in viestit:
        teksti = viesti.palauta_teksti().lower()
        maara = teksti.count(mjono)
        if maara != 0:
            nimi = viesti.palauta_nimi()
            if nimi in henk:
                henk[nimi] += maara
            else:
                henk[nimi] = maara
    summa = 0
    for nimi in henk:
        summa += henk[nimi]
    print("Merkkijonoa", mjono, "on käytetty", summa, "kertaa")
    for nimi in henk:
        print(nimi, "on käyttänyt merkkijonoa", mjono, henk[nimi], "kertaa")

def yleisimmat_sanat(viestit, maara, nayta_kaikki=False):
    sanalista = {}
    summa = 0
    for viesti in viestit:
        sanat = viesti.palauta_teksti().split(" ")
        for sana in sanat:
            if len(sana) < 2:
                continue
            summa += 1
            sana = sana.lower()
            if not sana[-1].isalpha() and sana[-2].isalpha():
                sana = sana[:-1]
            if sana in sanalista:
                sanalista[sana] += 1
            else:
                sanalista[sana] = 1
    print("Yhteensä eri sanoja", len(sanalista), "kappaletta")
    print("Viesteissä sanoja yhteensä", summa, "kappaletta")
    yhdiste = []
    for key in sanalista:
        yhdiste.append((key, sanalista[key]))
    yhdiste.sort(key=lambda x: x[1], reverse=True )
    if nayta_kaikki:
        maara = len(yhdiste)
    for i in range(maara):
        print(str(i+1)+".", yhdiste[i][0], "\tyhteensä", yhdiste[i][1],
               "kertaa")


    # yleisimmatm = [0]*maara
    # yleisimmats = [""]*maara
    # summa = 0
    # for sana in sanalista:
    #     luku = sanalista[sana]
    #     summa += luku
    #     if luku > yleisimmatm[-1]:
    #             for i in range(maara):
    #                 if luku > yleisimmatm[i]:
    #                     yleisimmatm.insert(i, luku)
    #                     yleisimmats.insert(i, sana)
    #                     yleisimmats.pop(-1)
    #                     yleisimmatm.pop(-1)
    #                     break
    # print("Viesteissä sanoja yhteensä", summa, "kappaletta")
    # print("Yleisimmät", maara, "sanaa:")
    # for i in range(maara):
    #     print(str(i+1)+".", yleisimmats[i], "\tyhteensä", yleisimmatm[i], \
    #           "kertaa")

def laske_henkiloiden_sanamaara(viestit):
    maara = {}
    viestimaara = {}
    for viesti in viestit:
        nimi = viesti.palauta_nimi()
        sanat = viesti.palauta_teksti().split(" ")
        luku = 0
        for sana in sanat:
            if len(sana) >1:
                luku += 1
        if nimi in maara:
            maara[nimi] += luku
        else:
            maara[nimi] = luku
        if nimi in viestimaara:
            viestimaara[nimi] += 1
        else:
            viestimaara[nimi] = 1
    for nimi in maara:
        print(nimi, "on kirjoittanut yhteensä", maara[nimi], "sanaa")
    print("Tällöin keskimääräinen viestin sanamäärä on henkilöillä seuraava:")
    for nimi in maara:
        suhde = maara[nimi]/viestimaara[nimi]
        print(nimi, "on kirjoittanut keskimäärin {:.2f} sanaa per viesti".format(suhde))

def pisimmmat_poissaolot(viestit):
    edellinen_viesti = {}
    pisin_tauko = {}
    i = 0
    for viesti in viestit:
        nimi = viesti.palauta_nimi()
        if nimi in edellinen_viesti:
            tauko = i-edellinen_viesti[nimi]-1
            if nimi in pisin_tauko:
                if tauko > pisin_tauko[nimi]:
                    pisin_tauko[nimi] = tauko
            else:
                pisin_tauko[nimi] = 0
        edellinen_viesti[nimi] = i
        i += 1
    for nimi in pisin_tauko:
        print(nimi, "on pisimmillään pitänyt", pisin_tauko[nimi], "viestin "
            "mittaisen tauon")

def taysin_viestiton_aika(viestit):
    edellinen_aika = viestit[0].palauta_aika()
    pisin_tauko = 0
    for viesti in viestit:
        tauko = viesti.palauta_aika().vali(edellinen_aika)
        if tauko > pisin_tauko:
            aloittaja = edellinen_viesti
            lopettaja = viesti
            pisin_tauko = tauko
        edellinen_aika = viesti.palauta_aika()
        edellinen_viesti = viesti
    paivat = pisin_tauko//(60*24)
    tunnit = (pisin_tauko-paivat*60*24)//60
    minuutit = pisin_tauko % 60
    print("Kaikista pisin tauko on ollut", paivat, "päivää,",
          tunnit, "tuntia ja", minuutit, "minuuttia pitkä")
    print("Tauon aloitti", aloittaja.palauta_nimi(), "viestillä",
          aloittaja.palauta_teksti(), aloittaja.palauta_aika(), "\nja lopetti", lopettaja.palauta_nimi(),
          "viestillä", lopettaja.palauta_teksti(), lopettaja.palauta_aika())

def kahden_kesken_kayty(viestit):
    pisin_ketju = 0
    kaksi_edellista = [" ", " "]
    keskustelijat = [" ", " "]
    i = 1
    for viesti in viestit:
        nimi = viesti.palauta_nimi()
        if nimi == kaksi_edellista[0]:
            i += 1
        elif nimi == kaksi_edellista[1]:
            i += 1
            kaksi_edellista[1] = kaksi_edellista[0]
            kaksi_edellista[0] = nimi
        else:
            if i > pisin_ketju:
                pisin_ketju = i
                keskustelijat[0] = kaksi_edellista[0]
                keskustelijat[1] = kaksi_edellista[1]
            i = 1
            kaksi_edellista[1] = kaksi_edellista[0]
            kaksi_edellista[0] = nimi
    if i > pisin_ketju:
        pisin_ketju = i
        keskustelijat = kaksi_edellista
    print("Kaikista pisin kahden keskustelijan välinen keskustelu on",
          pisin_ketju, "viestiä pitkä. Tämä käytiin henkilöiden",
          keskustelijat[0], "ja", keskustelijat[1], "välillä")

def keskinaiset_viestittelyt(viestit, minpituus):
    a = viestit[0].palauta_nimi()
    b = a
    viestittelijat = {}
    viestilkm = 0
    keskpituus = 0
    for viesti in viestit:
        if viesti.palauta_nimi() == a:
            viestilkm += 1
        elif viesti.palauta_nimi() == b:
            viestilkm += 1
            keskpituus += 1
            vali = a
            a = b
            b = vali
        else:
            if keskpituus > minpituus:
                if a > b:
                    nimipari = b + " ja " + a
                else:
                    nimipari = a + " ja " + b
                if nimipari in viestittelijat:
                    viestittelijat[nimipari] += viestilkm
                else:
                    viestittelijat[nimipari] = viestilkm
            keskpituus = 0
            viestilkm = 0
            b = a
            a = viesti.palauta_nimi()
    for pari in viestittelijat:
        print(pari, "ovat lähettäneet keskenään", viestittelijat[pari], "viestiä")
    print("Tulos on laskettu ottamalla vain huomioon yli", minpituus,
          "perättäistä keskinäistä viestittelyä")

def pisin_putki(viestit, minaika):
    edellinen = viestit[0].palauta_aika()
    alku = edellinen
    maxviesti = 0
    viestilkm = 0
    maxerotus = 0
    for viesti in viestit:
        aika = viesti.palauta_aika()
        erotus = aika.vali(edellinen)
        if erotus > minaika:
            if alku.vali(edellinen) > maxerotus:
                maxalku = alku
                maxloppu = edellinen
                maxviesti = viestilkm
            alku = viesti.palauta_aika()
            viestilkm = 0
        edellinen = aika
        viestilkm += 1

    print("Viestejä lähetetty enintään", minaika, "minuutin erolla on lähetetty "
          "yhtäjaksoisesti parhaimmillaan", maxalku, "ja", maxloppu, "välisen ajan" \
           "\nTällöin lähetettiin yhteensä", maxviesti, "viestiä")

def laske_henk_koht_kuukausi_viesti(viestit):
    alku = 12*viestit[0].palauta_vvvv() + viestit[0].palauta_kk()
    loppu = 12*viestit[-1].palauta_vvvv() + viestit[-1].palauta_kk()
    tyypit = {}
    i = 0
    for luku in range(alku - 1, loppu):
        kuukausi = luku % 12 + 1
        vuosi = luku // 12
        while True:
            if i < len(viestit) and viestit[i].palauta_kk() == kuukausi and \
                viestit[i].palauta_vvvv() == vuosi:
                tyyppi = viestit[i].palauta_nimi()
                if tyyppi in tyypit:
                    tyypit[tyyppi] += 1
                else:
                    tyypit[tyyppi] = 1
                i += 1

            else:
                print(str(kuukausi)+"/"+str(vuosi), end=' ')
                for tyyppi in tyypit:
                    print(tyypit[tyyppi], end=", ")
                    tyypit[tyyppi] = 0
                print()
                break
    for tyyppi in tyypit:
        print(tyyppi, end=",")

def main():
    # SUORITA SiistiTiedosto.py ENNEN TÄTÄ TIEDOSTOLLE!!!
    aika1 = time.time()
    tiedosto = "Pösteridata2017b.txt"
    alpvm = Aika(25, 11, 2016, 0, 0)
    lopvm = Aika(25, 11, 2017, 23, 59)
    viestit = muodosta_viestit(tiedosto, alpvm, lopvm)
    print("Tiedot ajalta", alpvm, "-",  lopvm)
    # Käytettävät funktiot: laske_nimikohtainen(), laske_kuukausikohtainen()
    # laske_paivakohtainen(), laske_tuntikohtainen(), laske_minuuttikohtainen()
    # etsi_pisin_viesti(), onko_rivinvaihtoja(), viesteja_lahetettu(),
    # laske_merkkijonofrekvenssi(mjono), yleisimmat_sanat(maara),
    # laske_henkiloiden_sanamaara(), pisimmat_poissaolot, taysin_viestiton_aika
    # kahden_kesken_kayty(), keskinaiset__viestittelyt(minpituus),
    # pisin_putki(minaika minuutteina)

    #viesteja_lahetettu(viestit)
    #laske_nimikohtainen(viestit, alpvm, lopvm)
    #etsi_pisin_viesti(viestit)
    yleisimmat_sanat(viestit, 100, nayta_kaikki=False)
    #laske_henkiloiden_sanamaara(viestit)
    #pisimmmat_poissaolot(viestit)
    #taysin_viestiton_aika(viestit)
    #kahden_kesken_kayty(viestit)
    #keskinaiset_viestittelyt(viestit, 10)
    #pisin_putki(viestit, 600) ei taida toimia
    #laske_merkkijonofrekvenssi(viestit, "..l..")
    #laske_kuukausikohtainen(viestit)
    #laske_paivakohtainen(viestit)
    #laske_tuntikohtainen(viestit)
    #laske_minuuttikohtainen(viestit)
    #laske_henk_koht_kuukausi_viesti(viestit)
    aika2 = time.time()

    print(aika2-aika1)

    print(viestit[0].palauta_aika(), viestit[-1].palauta_aika())

main()