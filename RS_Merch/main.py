__author__ = 'Joonas'

import time
import webbrowser

VALMIIT_OSTOKSET = []
KESKEYTETYT_OSTOKSET = []
KESKENERAISET_OSTOKSET = []
RAHAMAARA = 10
RAHOIHINMUUTOS = False

class Ostos:
    def __init__(self, tiedot):
        self.nimi = tiedot[0]
        self.pvm = tiedot[1]
        self.maara = tiedot[2]
        self.hinta = tiedot[3]
        self.myyty = tiedot[4]
        self.liikevaihto = tiedot[5]
        self.voitto = tiedot[6]
        self.loppunut = tiedot[7]
        self.keskeytetty = tiedot[8]

    def osta_lisaa(self, maara, hinta):
        self.maara += maara
        self.hinta += hinta
        print("Materiaalia", self.nimi, "on ostettu yhteensä", luku_muoto(self.maara),
              "hintaan", luku_muoto(self.hinta))

    def myy(self, maara, hinta):
        self.myyty += maara
        self.liikevaihto += hinta
        rahoihin_muutos(hinta - self.hinta/self.maara*maara)

        if self.myyty >= self.maara:
            self.loppunut = hae_pvm()
            self.voitto = self.liikevaihto - self.hinta
            VALMIIT_OSTOKSET.append(self)
            KESKENERAISET_OSTOKSET.remove(self)
            print("Kaikki myyty, voittoa kaupoista:", luku_muoto(self.voitto))

    def keskeyta(self):
        self.keskeytetty = True
        KESKEYTETYT_OSTOKSET.append(self)
        KESKENERAISET_OSTOKSET.remove(self)

    def laske_vp(self):
        return self.voitto/self.hinta

    def tulosta_tiedot(self):
        print("\nNimi:", self.nimi)
        print("Aika:", self.pvm)
        print("Määrä:", luku_muoto(self.maara))
        print("Hinta:", luku_muoto(self.hinta))
        print("Kappalehinta:", round(self.hinta/self.maara, 2))
        print("Myyty:", luku_muoto(self.myyty))
        print("Myyntihinta:", luku_muoto(self.liikevaihto))
        if self.myyty > 0:
            print("Myyntihinta kappaleelta", round(self.liikevaihto/self.myyty, 2))
        if self.loppunut != "" :
            print("Loppunut: " + self.loppunut)
            print("Voitto:", luku_muoto(self.voitto))
            print("Voittoprosentti:", round(self.voitto/self.hinta*100, 2))
            print("Tienesti päivässä: ", luku_muoto(self.laske_paivatienesti()), "\n")

    def palauta_tallennusmuoto(self):
        palautus = ""
        palautus += "nimi: " + self.nimi + "\n"
        palautus += "aika: " + self.pvm + "\n"
        palautus += "maara: " + str(self.maara) + "\n"
        palautus += "hinta: " + str(self.hinta) + "\n"
        palautus += "myyty: " + str(self.myyty) + "\n"
        palautus += "liikevaihto: " + str(self.liikevaihto) + "\n"
        palautus += "voitto: " + str(self.voitto) + "\n"
        palautus += "loppunut: " + self.loppunut + "\n"
        num2 = "0"
        if self.keskeytetty:
            num2 = "1"
        palautus += "keskeytetty: " + num2 + "\n"
        palautus += "#"*10+"\n"
        return palautus

    def myy_kaikki(self, hinta):
        self.myy(self.maara-self.myyty, hinta)

    def laske_paivatienesti(self):
        kkpvm = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        tiedot1 = self.pvm.split(".")
        tiedot2 = self.loppunut.split(".")
        vali = int(tiedot2[0]) - int(tiedot1[0])
        while tiedot1[1] != tiedot2[1] or tiedot1[2] != tiedot2[2]:
            vali += kkpvm[int(tiedot1[1]) - 1]
            tiedot1[1] = str(int(tiedot1[1]) + 1)
            if tiedot1[1] == "13":
                tiedot1[1] = "1"
                tiedot1[2] = str(int(tiedot1[2]) + 1)
        if vali != 0:
            return self.voitto // vali
        else:
            return self.voitto

def luo_ostokset():
    tiedosto = open("kaupat2.txt", "r")
    tiedot = []
    for rivi in tiedosto:
        rivi = rivi.rstrip()
        if rivi == "##########":
            if tiedot[7] != "":
                VALMIIT_OSTOKSET.append(Ostos(tiedot))
            elif tiedot[8]:
                KESKEYTETYT_OSTOKSET.append(Ostos(tiedot))
            else:
                KESKENERAISET_OSTOKSET.append(Ostos(tiedot))
            tiedot = []
        elif rivi[:5] == "nimi:":
            tiedot.append(rivi[6:])
        elif rivi[:5] == "aika:":
            tiedot.append(rivi[6:])
        elif rivi[:6] == "maara:":
            tiedot.append(int(rivi[7:]))
        elif rivi[:6] == "hinta:":
            tiedot.append(int(rivi[7:]))
        elif rivi[:6] == "myyty:":
            tiedot.append(int(rivi[7:]))
        elif rivi[:12] == "liikevaihto:":
            tiedot.append(int(rivi[13:]))
        elif rivi[:7] == "voitto:":
            tiedot.append(int(rivi[8:]))
        elif rivi[:9] == "loppunut:":
            tiedot.append(rivi[10:])
        elif rivi[:12] == "keskeytetty:":
            tiedot.append(int(rivi[13:]) == 1)
        else:
            print("TIEDOSTOSSA VIRHE")
            print(rivi)
    tiedosto.close()
    tiedosto = open("rahat.txt", "r")
    for rivi in tiedosto:
        pass
    tiedosto.close()
    rivi = rivi.rstrip()
    luku = ""
    tallenna = False
    for a in rivi:
        if tallenna:
            luku += a
        elif a == " ":
            tallenna = True

    global RAHAMAARA
    RAHAMAARA = int(luku)

def tallenna_ostokset():
    tuloste = ""
    for i in KESKENERAISET_OSTOKSET:
        tuloste += i.palauta_tallennusmuoto()
    for i in VALMIIT_OSTOKSET:
        tuloste += i.palauta_tallennusmuoto()
    for i in KESKEYTETYT_OSTOKSET:
        tuloste += i.palauta_tallennusmuoto()
    tiedosto = open("kaupat2.txt", "w")
    tiedosto.write(tuloste)
    tiedosto.close()

def hae_pvm():
        a = time.localtime()
        palautus = str(a[2])+"."+str(a[1])+"."+str(a[0])
        return palautus

def listaa_keskeneraiset():
    for i in range(len(KESKENERAISET_OSTOKSET)):
        print("\nPaikka: ", i, "\n")
        KESKENERAISET_OSTOKSET[i].tulosta_tiedot()
    if len(KESKENERAISET_OSTOKSET) == 0:
        print("Ei keskeneräisiä ostoksia")
        return
    ostos = 0
    while True:
        try:
            a = int(input("Paikan numero: "))
            ostos = KESKENERAISET_OSTOKSET[a]
            break
        except ValueError:
            print("ANNA LUKU")
        except IndexError:
            print("VÄÄRÄ LUKU")
    while True:
        syote = input("Valitse toimenpide (m, o, ke, mk, exit): ")

        if syote == "m" or syote == "o":
            while True:
                try:
                    maara = int(input("Määrä: "))
                    hinta = int(input("Hinta: "))
                    if syote == "m":
                        ostos.myy(maara, hinta)
                    else:
                        ostos.osta_lisaa(maara, hinta)
                    return
                except ValueError:
                    print("Anna luku")
        elif syote == "ke":
            ostos.keskeyta()
            return
        elif syote == "mk":
            try:
                hinta = int(input("Hinta: "))
                ostos.myy_kaikki(hinta)
                return
            except ValueError:
                print("Meni väärin! Perutaan toiminto.")
        elif syote == "exit":
            return
        else:
            print("Virheellinen komento")

def listaa_parhaat():
    plista = []
    while True:
        syote = input("Voitto vaiko voittoprosentti vaiko paivavoitto (vo, vp, pvmvo): ")
        if syote == "vo":
            plista = sorted(VALMIIT_OSTOKSET, key=lambda o: o.voitto,
                            reverse= False)
            break
        elif syote == "vp":
            plista = sorted(VALMIIT_OSTOKSET, key=lambda o: o.laske_vp(),
                            reverse= False)
            break
        elif syote == "pvmvo":
            plista = sorted(VALMIIT_OSTOKSET, key=lambda o: o.laske_paivatienesti(),
                            reverse= False)
            break
        else:
            print("Virheellinen komento!")
    a = len(plista)
    for i in plista:
        print("Sija:", a)
        a -= 1
        i.tulosta_tiedot()

def luku_muoto(luku):
    i = -3
    palautus = str(luku)
    while True:
        if i < -len(palautus)+1:
            break
        palautus = palautus[:i] + " " + palautus[i:]
        i -= 4
    return palautus

def uusi_ostos():
    nimi = input("Tuotteen nimi: ")
    pvm = hae_pvm()
    while True:
        maara = input("Ostettu maara: ")
        try:
            maara = int(maara)
            break
        except ValueError:
            print("Virheellinen syöte!")
    while True:
        hinta = input("Maksettu hinta: ")
        try:
            hinta = int(hinta)
            break
        except ValueError:
            print("Virheellinen syöte!")
    if maara == 0:
        return
    tiedot = (nimi, pvm, maara, hinta, 0, 0, 0, "", False)
    uostos = Ostos(tiedot)
    KESKENERAISET_OSTOKSET.append(uostos)
    tallenna_ostokset()
    uostos.tulosta_tiedot()

def tallenna_rahat():
    kirjoitus = "\n" + hae_pvm() + " " + str(RAHAMAARA)
    tiedosto = open("rahat.txt", "a")
    tiedosto.write(kirjoitus)
    tiedosto.close()

def rahoihin_muutos(muutos):
    global RAHAMAARA
    muutos = round(muutos/1000.0, 0)
    if muutos == 0.0:
        return
    RAHAMAARA += int(muutos*1000)
    global RAHOIHINMUUTOS
    RAHOIHINMUUTOS = True

def muuta_rahamaaraa():
    while True:
        try:
            rahat = int(input("Uusi rahamäärä: "))
            rahoihin_muutos(rahat - RAHAMAARA)
            return
        except ValueError:
            print("Yritä uudestaan")

def avaa_rune():
    webbrowser.open('rs-launch://www.runescape.com/k=5/l=$(Language:0)/jav_config.ws')
    return

def main():
    avaa_rune()
    luo_ostokset()
    while True:
        syote = input("Valitse toimenpide (uusi, listaa, parhaat, rahat, muutaraha, exit): ")
        if syote == "uusi":
            uusi_ostos()
        elif syote == "listaa":
            listaa_keskeneraiset()
            tallenna_ostokset()
        elif syote == "parhaat":
            listaa_parhaat()
        elif syote == "exit":
            tallenna_ostokset()
            if RAHOIHINMUUTOS:
                tallenna_rahat()
            break
        elif syote == "rahat":
            print("Omaisuuden arvo noin:", luku_muoto(RAHAMAARA))
        elif syote == "muutaraha":
            muuta_rahamaaraa()
        else:
            print("Viheellinen komento!")

main()
