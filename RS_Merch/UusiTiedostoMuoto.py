__author__ = 'Joonas'

def main():
    tiedosto = open("kaupat.txt", "r")
    paivamaaraLista = []
    tuloste = []
    for rivi in tiedosto:
        r = rivi.strip()
        tuloste.append(r)
        if r[:5] == "aika:":
            paivamaaraLista.append(r[6:])
    tiedosto.close()
    a = 1
    for i in range(len(tuloste)):
        r = tuloste[i]
        if r == "loppunut: 1":
            tuloste[i] = "loppunut: " + paivamaaraLista[a]
            a = (a + 1) % len(paivamaaraLista)
        elif r == "loppunut: 0":
            tuloste[i] = "loppunut: "
            a = (a + 1) % len(paivamaaraLista)
    tiedosto = open("kaupat2.txt", "w")
    for rivi in tuloste:
        r = rivi + "\n"
        tiedosto.write(r)
    tiedosto.close()
main()