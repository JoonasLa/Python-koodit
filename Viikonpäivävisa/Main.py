__author__ = 'Joonas'
import random
from tkinter import *
from tkinter import messagebox
import time
import datetime

VIIKONPAIVAT = ["maanantai", "tiistai", "keskiviikko", "torstai", "perjantai", "lauantai", "sunnuntai"]
KUUKAUSIPVM = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
APUTAULUKKO = [0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5]
ALKU = 1900
LOPPU = 2000

class Paivamaara:
    def __init__(self, pvm, kk, vvvv):
        self.__pvm = pvm
        self.__kk = kk
        self.__vvvv = vvvv

    def pvm(self):
        return self.__pvm

    def kk(self):
        return self.__kk

    def vvvv(self):
        return self.__vvvv

    def palauta_teksti(self):
        return (str(self.__pvm) + "." + str(self.__kk) + "."+str(self.__vvvv))

    @staticmethod
    def arvo_pvm(alku, loppu):
        vuosi = random.randint(alku, loppu)
        kuukausi = random.randint(0, 11)
        if kuukausi == 2 and on_karkausvuosi:
            paiva = random.randint(1, 30)
        else:
            paiva = random.randint(1, KUUKAUSIPVM[kuukausi])
        kuukausi += 1
        return(Paivamaara(paiva, kuukausi, vuosi))

class Tulokset:
    def __init__(self, pvm, oikein, aika):
        if pvm == "":
            self.__pvm = str(datetime.datetime.now().date())
        else:
            self.__pvm = pvm
        self.__oikein = oikein
        self.__aika = aika

    def oikein(self):
        return self.__oikein

    def aika(self):
        return self.__aika

    def pvm(self):
        return self.__pvm

    def suhde(self):
        oikein = int(self.__oikein)
        if oikein == 0:
            return 999 + float(self.__aika)
        else:
            return float(self.__aika) / oikein

    def muuta_tekstimuotoon(self):
        return self.__pvm + ":" + str(self.__oikein) + ":" + str(self.__aika)

    def tallenna(self):
        tiedosto = open("Tulokset.txt", "a")
        tiedosto.write(self.muuta_tekstimuotoon() + "\n")
        tiedosto.close()

    @staticmethod
    def lue_vanhat_tulokset(self):
        tiedosto = open("Tulokset.txt", "r")
        palautus = []
        lista = []
        for i in tiedosto:
            rivi = i.rstrip()
            if len(rivi) == 0:
                continue
            tiedot = rivi.split(":")
            lista.append(Tulokset(tiedot[0], tiedot[1], tiedot[2]))
        for tulos in lista:
            if len(palautus) == 0:
                palautus.append(tulos)
                continue
            vika = True
            for i in range(len(palautus)):
                if tulos.suhde() < palautus[i].suhde():
                    palautus.insert(i, tulos)
                    vika = False
                    break
            if vika:
                palautus.append(tulos)
        return palautus

def on_karkausvuosi(vuosi):
    return vuosi % 4 == 0 and (vuosi % 100 != 0 or vuosi % 400 == 0)

def laske_viikonpaiva(paiva):
    luku = 0
    vvvv = paiva.vvvv()
    kk = paiva.kk()
    pvm = paiva.pvm()
    if vvvv >= 1900:
        for vuosi in range(1900, vvvv):
            if on_karkausvuosi(vuosi):
                luku += 2
            else:
                luku += 1
    else:
        for vuosi in range(1899, vvvv-1, -1):
            if on_karkausvuosi(vuosi):
                luku -= 2
            else:
                luku -= 1
    if on_karkausvuosi(vvvv) and kk > 2:
       luku += 1
    luku = luku % 7
    for kuukausi in range(0, kk-1):
        luku += KUUKAUSIPVM[kuukausi]
    luku += pvm-1
    luku = luku % 7
    return VIIKONPAIVAT[luku]

class Käyttöliittymä:
    def __init__(self, root):
        self.__main = Frame(root)
        self.__main.grid(row=0, column=0)
        self.__paiva = Paivamaara.arvo_pvm(ALKU, LOPPU)
        self.__paivatext = Label(self.__main, text=self.__paiva.palauta_teksti())
        self.__paivatext.grid(row = 0, column = 0)

        self.__paivanapit = []
        self.__nappiframe = Frame(self.__main)
        for i in range(7):
            nappi = Button(self.__nappiframe, text=VIIKONPAIVAT[i], command=lambda i=i: self.vastaa(i))
            nappi.grid(row=0, column=i)
            self.__paivanapit.append(nappi)
        self.__nappiframe.grid(row=1, column=0)
        self.__oikeinteksti = Label(self.__main, text="")
        self.__oikeinteksti.grid(row=2, column=0)

        self.__aloitusaika = time.time()
        self.__vuoro = 0
        self.__vuorolabel = Label(self.__main, text=str(self.__vuoro+1))
        self.__vuorolabel.grid(row=0, column=1)
        self.__oikeat_vastaukset = 0
        self.__vanhat_tulokset = Tulokset.lue_vanhat_tulokset(self)
        self.__resetoi_nappi = Button(self.__main, text="RESET", command=self.resetoi)
        self.__resetoi_nappi.grid(row=2, column = 1)
        self.__aputaulukko = Label(self.__main, text=str(APUTAULUKKO))
        self.__aputaulukko.grid(row=3, column=0)


    def vastaa(self, luku):
        if VIIKONPAIVAT[luku] == laske_viikonpaiva(self.__paiva):
            self.__oikeinteksti["text"] = "Vastasit oikein!"
            self.__oikeat_vastaukset += 1
        else:
            self.__oikeinteksti["text"] = "Vastasit väärin!"
        self.__paiva = Paivamaara.arvo_pvm(ALKU, LOPPU)
        self.__paivatext["text"] = self.__paiva.palauta_teksti()
        self.__vuoro += 1
        if self.__vuoro == 5:
            self.peli_loppui()

        self.__vuorolabel["text"] = self.__vuoro+1

    def peli_loppui(self):
        aika = round(time.time()-self.__aloitusaika, 2)
        aikaa = "{:.2f}".format(aika)
        tulos = Tulokset("", self.__oikeat_vastaukset, aikaa)
        if aika > 10:
            tulos.tallenna()
        sijoitus = len(self.__vanhat_tulokset) + 1
        löyty = False
        for i in range(len(self.__vanhat_tulokset)):
            if tulos.suhde() < self.__vanhat_tulokset[i].suhde():
                self.__vanhat_tulokset.insert(i, tulos)
                sijoitus = i + 1
                löyty = True
                break
        if not löyty:
            self.__vanhat_tulokset.append(tulos)
        toplista = ""
        for i in range(min(len(self.__vanhat_tulokset), 10)):
            tulos = self.__vanhat_tulokset[i]
            toplista += tulos.muuta_tekstimuotoon()+"\n"

        teksti = "Sait " + str(self.__oikeat_vastaukset) + " oikein ajassa " + \
                  str(aikaa) + "\nTällä tuloksella pääset sijalle " + \
                 str(sijoitus) + "\nTOP 10 LISTA:\n" + toplista
        messagebox.showinfo("Peli päättyi", teksti)
        self.__vuoro = 0
        self.__oikeat_vastaukset = 0
        self.__vuorolabel["text"] = 1
        self.__aloitusaika = time.time()

    def resetoi(self):
        self.__aloitusaika = time.time()
        self.__oikeat_vastaukset = 0
        self.__vuoro = 0
        self.__vuorolabel["text"] = 1
        self.__paiva = Paivamaara.arvo_pvm(ALKU, LOPPU)
        self.__paivatext["text"] = self.__paiva.palauta_teksti()
        

def main():
    root = Tk()
    root.title("Arvaa viikonpäivä")
    Käyttöliittymä(root)
    root.mainloop()

main()
