from math import exp
from random import random, randint

MAX_LAYERS = 5
MIN_LAYERS = 1
MAX_NEURONS = 2000
MIN_NEURONS = 5



def sigmoid(x):
    return 1/(1 + exp(-x))

class Matrix:
    def __init__(self, m, n):
        self.__m = m
        self.__n = n
        self.__matrix = []
        for i in range(m):
            mat = [None] * n
            self.__matrix.append(mat)

    @staticmethod
    def times(first, other):
        if other.__m != first.__n:
            raise ValueError
        result = Matrix(first.__m, other.__n)
        for i in range(first.__m):
            for j in range(other.__n):
                su = 0
                for k in range(first.__n):
                    su += first.__matrix[i][k] * other.__matrix[k][j]
                result.__matrix[i][j] = su
        return result

    def randomize(self, mini=0, maxi=1):
        for i in range(self.__m):
            for j in range(self.__n):
                self.__matrix[i][j] = randint(mini, maxi)

    @staticmethod
    def copy(other):
        mat = Matrix(other.__m, other.__n)
        for j in range(other.__m):
            for i in range(other.__n):
                mat.__matrix[j][i] = other.__matrix[j][i]
        return mat

    @staticmethod
    def array2matrix(array):
        mat = Matrix(len(array), 1)
        for i in range(len(array)):
            mat.__matrix[i][0] = array[i]
        return mat

    @staticmethod
    def array2matrix2D(array):
        mat = Matrix(len(array), len(array[0]))
        for j in range(len(array)):
            for i in range(len(array[j])):
                mat.__matrix[j][i] = array[j][i]
        return mat


    @staticmethod
    def matrix2array(matrix):
        array = []
        for j in range(matrix.__m):
            array.append(matrix.__matrix[j][0])
        return array

    def change(self, mini=-1, maxi=1, only_positive=False):
        if only_positive:
            for j in range(self.__m):
                for i in range(self.__n):
                    self.__matrix[j][i] = max(0, randint(mini, maxi)
                                              + self.__matrix[j][i])
        else:
            for j in range(self.__m):
                for i in range(self.__n):
                    self.__matrix[j][i] += randint(mini, maxi)

    def change_size(self, m_change, n_change):
        if m_change > 0:
            for j in range(m_change):
                mat = []
                y = j
                for i in range(self.__n):
                    mat.append(self.__matrix[y][i])
                    y = (y + 1) % self.__m
                self.__matrix.append(mat)
            self.__m += m_change

        elif m_change < 0:
            self.__matrix = self.__matrix[:m_change]
            self.__m += m_change

        if n_change > 0:
            for j in range(self.__m):
                x = j
                for i in range(n_change):
                    self.__matrix[j].append(self.__matrix[j][(x + i) % self.__n])
            self.__n += n_change

        elif n_change < 0:
            for j in range(self.__m):
                self.__matrix[j] = self.__matrix[j][:n_change]
            self.__n += n_change

    def minus(self, other):
        if self.__m != other.__m or self.__n != other.__n:
            raise ValueError
        for j in range(self.__m):
            for i in range(self.__n):
                self.__matrix[j][i] -= other.__matrix[j][i]

    def sigmoid_transform(self):
        for j in range(self.__m):
            for i in range(self.__n):
                self.__matrix[j][i] = sigmoid(self.__matrix[j][i])

    def delete_negatives(self):
        for i in range(self.__m):
            for j in range(self.__n):
                if self.__matrix[i][j] < 0:
                    self.__matrix[i][j] = 0
                else:
                    self.__matrix[i][j] = 1

    def __str__(self):
        res = ""
        for j in range(self.__m):
            for i in range(self.__n):
                res += str(self.__matrix[j][i]) + " "
            res += "\n"
        return res

    def transpose_str(self):
        res = ""
        for i in range(self.__n):
            for j in range(self.__m):
                res += str(self.__matrix[j][i]) + " "
            res += "\n"
        return res


class Tekoaly:
    ALYCOUNTER = 0

    def __init__(self, layers):
        self.__weights = []
        self.__biases = []
        self.__counter = Tekoaly.ALYCOUNTER
        self.__layers = layers
        Tekoaly.ALYCOUNTER += 1

        for l in layers[1:]:
            bia = Matrix(l, 1)
            self.__biases.append(bia)

        for l1, l2 in zip(layers, layers[1:]):
            w = Matrix(l2, l1)
            self.__weights.append(w)

    def __str__(self):
        return "Aly" + str(self.__counter)

    def __repr__(self):
        return str(self)

    def random(self):
        for layer in range(len(self.__weights)):
            self.__weights[layer].randomize(-1, 1)
            self.__biases[layer].randomize(0, 3)

    def copy(self, another):
        self.__weights = []
        self.__biases = []
        self.__layers = another.__layers[:]
        for l in range(len(self.__layers) - 1):
            self.__weights.append(Matrix.copy(another.__weights[l]))
            self.__biases.append(Matrix.copy(another.__biases[l]))

    def mutate(self):
        change_layers = random()
        for l in range(len(self.__layers)-2):
            change_neurons = random()
            if change_neurons < 0.3 and self.__layers[l + 1] > MIN_NEURONS:
                self.__layers[l + 1] -= 1
                self.__biases[l].change_size(-1, 0)
                self.__weights[l].change_size(-1, 0)
                self.__weights[l + 1].change_size(0, -1)
            elif change_neurons > 0.7 and self.__layers[l + 1] < MAX_NEURONS:
                self.__layers[l + 1] += 1
                self.__biases[l].change_size(1, 0)
                self.__weights[l].change_size(1, 0)
                self.__weights[l + 1].change_size(0, 1)

            self.__biases[l].change(-3, 3, only_positive=True)
            self.__weights[l].change(-2, 2)
        self.__biases[-1].change(-3, 3, only_positive=True)
        self.__weights[-1].change(-2, 2)

    def choose(self, game):
        board, player = game.palauta_tilanne()
        vector = game.palauta_alylle(player)
        for l in range(len(self.__biases)):
            vector = Matrix.times(self.__weights[l], vector)
            vector.minus(self.__biases[l])
            vector.delete_negatives()

        result = Matrix.matrix2array(vector)
        order = []
        for i, val in enumerate(result):
            tup = [i, val]
            order.append(tup)

        while True:
            move = max(order, key=lambda x: x[1])
            if game.voi_siirtaa(move[0]):
                return move[0]
            else:
                order[move[0]][1] = -1

    def save(self, savename):
        file = open(savename, 'w')
        file.write("Layer size\n")
        line = ""
        for size in self.__layers:
            line += str(size) + " "
        file.write(line + "\n")
        file.write("Biases\n")
        for bias in self.__biases:
            file.write(bias.transpose_str())

        file.write("Weigths\n")
        for weight in self.__weights:
            file.write(str(weight) + "\n")
        file.close()

    @staticmethod
    def load(loadname):
        file = open(loadname, 'r')
        file.readline()
        sizes = file.readline().strip().split(" ")
        for i in range(len(sizes)):
            sizes[i] = int(sizes[i])

        file.readline()
        aly = Tekoaly(sizes)
        for i in range(len(sizes) - 1):
            biases = file.readline().strip().split(" ")
            bias = []
            for j in biases:
                bias.append(int(j))
            aly.__biases[i] = Matrix.array2matrix(bias)

        file.readline()

        k = 0
        for m, n in zip(sizes[1:], sizes):
            mat = []
            for j in range(m):
                weights = file.readline().strip().split(" ")
                for i in range(n):
                    weights[i] = int(weights[i])
                mat.append(weights)
            aly.__weights[k] = Matrix.array2matrix2D(mat)
            file.readline()
            k += 1
        file.close()
        return aly

    @staticmethod
    def random_aly():
        size = int(MIN_LAYERS + random() * (MAX_LAYERS - MIN_LAYERS + 1))
        layers = [14]
        for i in range(size):
            neurons = int(MIN_NEURONS + random() *
                          (MAX_NEURONS - MIN_NEURONS + 1))
            layers.append(neurons)
        layers.append(6)
        aly = Tekoaly(layers)
        aly.random()
        return aly

    @staticmethod
    def copy_aly(another):
        layers = []
        for i in another.__layers:
            layers.append(i)
        aly = Tekoaly(layers)
        aly.copy(another)
        return aly
