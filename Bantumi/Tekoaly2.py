__author__ = 'Joonas'

from math import exp
from random import random, randint
from copy import copy as cp
def sigmoid(x):
    return 1/(1 + exp(-x))

def step(x):
    if x < 0:
        return 0
    return 1

MIN_NEURONS = 5
MAX_NEURONS = 200
MIN_AFFECTING = 1
MAX_AFFECTING = MAX_NEURONS
MIN_LAYERS = 1
MAX_LAYERS = 5

class Neuron():
    def __init__(self, init_val=None, prev_neurons=None, aff_neurons=None):
        self.__value = init_val
        self.__previous_neurons = prev_neurons
        self.__weights = aff_neurons
        self.__bias = None

    def randomize(self):
        self.__weights = {}
        p = len(self.__previous_neurons)
        n = randint(MIN_AFFECTING, min(p, MAX_AFFECTING))
        if p % n == 0:
            a = randint(0, len(self.__previous_neurons) // n - 1)
        else:
            a = randint(0, len(self.__previous_neurons) // n)
        for i in range(a, p, p // n):
            self.__weights[self.__previous_neurons[i]] = \
                (random() - 0.5) * 2
        self.__bias = random() * n / 3

    def calc_value(self):
        v = 0
        for neuron in self.__weights:
            v += neuron.__value * self.__weights[neuron]
        self.__value = sigmoid(v - self.__bias)

    def copy(self):
        c = Neuron(init_val=self.__value, prev_neurons=cp(self.__previous_neurons),
                   aff_neurons=cp(self.__weights))
        c.__bias = self.__bias
        return c

    def mutate(self):
        pass

    def __str__(self):
        return str(self.__bias) + " " + str(self.__weights)

class Tekoaly2():
    def __init__(self, layers):
        self.__layers = layers
        lay = []
        for i in range(14):
            lay.append(Neuron())
        self.__neurons.append(lay)

        for l in layers[1:]:
            lay = []
            for i in range(l):
                lay.append(Neuron(prev_neurons=self.__layers[-1]))
            self.__neurons.append(lay)

    def save(self, savename):
        file = open(savename, 'w')
        file.write("Layers\n")
        for l in self.__layers:
            file.write(str(l) + " ")
        file.write("\nNeurons\n")
        for neu in self.__neurons:
            for n in neu:
                self.__neu



