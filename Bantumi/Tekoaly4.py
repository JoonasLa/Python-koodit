__author__ = 'Joonas'
from random import random, randint
from Bantumi import Bantumi, pelaa_ilman_piirtoa, syote_satunnaisesti, \
    syote_algoritmilta, syote_pelaajalta
from copy import deepcopy

inf = float('inf')


MAX_NEURONS = 1000


class Neuron():

    def __init__(self, rand=True):
        self.__input = []
        self.__output = []
        self.__bias = 0
        self.__times_activated = 0
        self.__activation_value = 0
        self.__total_times_activated = 0
        if rand:
            self.randomize()

    def randomize(self):
        limit = random()
        for i in range(14):
            r = random()
            if r < limit:
                w = (i, (r / limit) * 2 - 1)
                self.__input.append(w)
        if len(self.__input) == 0:
            w = (randint(0, 13), random() * 2 - 1)
            self.__input.append(w)

        limit = random()
        for i in range(6):
            r = random()
            if r < limit:
                w = (i, (r / limit) * 2 - 1)
                self.__output.append(w)
        if len(self.__output) == 0:
            w = (randint(0, 5), random() * 2 - 1)
            self.__output.append(w)

        self.__bias = random() * 2 - 1

    def set_neuron(self, line):
        inputs, outputs, bias = line.split("|")
        inputs = inputs.split(" ")
        for l in inputs:
            try:
                i, w = l.split(":")
                self.__input.append((int(i), float(w)))
            except ValueError:
                continue
        outputs = outputs.split(" ")
        for l in outputs:
            i, w = l.split(":")
            self.__output.append((int(i), float(w)))
        self.__bias = float(bias)

    def choose(self, inp, output):
        s = 0
        for i, w in self.__input:
            s += inp[i] * w
        if s > self.__bias:
            for i, w in self.__output:
                output[i] += w
            self.__times_activated += 1

    def no_activation(self):
        return self.__total_times_activated == 0

    def mark_value(self, bool_won):
        if bool_won:
            self.__activation_value += self.__times_activated
        else:
            self.__activation_value -= self.__times_activated
        self.__total_times_activated += self.__times_activated
        self.__times_activated = 0

    def return_value(self):
        return self.__activation_value

    def reset_activations(self):
        self.__activation_value = 0
        self.__times_activated = 0
        self.__total_times_activated = 0

    def __str__(self):
        line = ""
        for i, w in self.__input:
            line += str(i) + ":" + str(w) + " "
        line = line[:-1] + "|"
        for i, w in self.__output:
            line += str(i) + ":" + str(w) + " "
        line = line[:-1] + "|" + str(self.__bias)
        return line

    @staticmethod
    def copy(another):
        neuron = Neuron(rand=False)
        neuron.__output = deepcopy(another.__output)
        neuron.__input = deepcopy(another.__input)
        neuron.__bias = another.__bias
        return neuron


class Tekoaly4():

    def __init__(self, neuron_count=50):
        self.__neurons = []
        for i in range(neuron_count):
            self.__neurons.append(Neuron())

    def choose(self, game: Bantumi):
        board, player = game.palauta_tilanne()
        board = game.palauta_list(player)
        options = [None] * 6
        for i in range(6):
            if board[i] == 0:
                options[i] = -inf
            else:
                options[i] = 0
        for n in self.__neurons:
            n.choose(board, options)
        maxi = 0
        maxv = options[0]
        for i in range(1, 6):
            if options[i] > maxv:
                maxv = options[i]
                maxi = i
        return maxi

    def save(self, savename):
        file = open(savename, 'w')
        for n in self.__neurons[:-1]:
            file.write(str(n) + "\n")
        file.write(str(self.__neurons[-1]))
        file.close()

    def mark_neurons(self, bool_won):
        for n in self.__neurons:
            n.mark_value(bool_won)

    def mutate(self, print_info=False):
        newlist = []
        for n in self.__neurons:
            if not n.no_activation() and n.return_value() >= 0:
                newlist.append(n)
            n.reset_activations()
        if print_info:
            print("Neuroneja", len(self.__neurons), "Suodatuksen jalkeen",
                  len(newlist))
        for i in range(10):
            newlist.append(Neuron())
        self.__neurons = newlist

    def add_neuron(self):
        self.__neurons.append(Neuron())

    def change_last(self):
        self.__neurons[-1] = Neuron()

    def print_size(self):
        print("Neuroneja yhteensa:", len(self.__neurons))

    @staticmethod
    def load(loadname):
        aly = Tekoaly4(neuron_count=0)
        file = open(loadname, 'r')
        for line in file:
            neuron = Neuron(rand=False)
            line = line.strip()
            neuron.set_neuron(line)
            aly.__neurons.append(neuron)
        file.close()
        return aly

    @staticmethod
    def copy(another):
        aly = Tekoaly4(neuron_count=0)
        for n in another.__neurons:
            aly.__neurons.append(Neuron.copy(n))
        return aly

    @staticmethod
    def add_together(aly1, aly2):
        aly = Tekoaly4(0)
        for n in aly1.__neurons:
            aly.__neurons.append(Neuron.copy(n))
        for n in aly2.__neurons:
            aly.__neurons.append(Neuron.copy(n))
        return aly

    @staticmethod
    def combine(aly1, aly2):
        aly = Tekoaly4(0)
        for n in aly1.__neurons:
            if n.return_value() > 0:
                aly.__neurons.append(Neuron.copy(n))
        for n in aly2.__neurons:
            if n.return_value() > 0:
                aly.__neurons.append(Neuron.copy(n))
        return aly

    @staticmethod
    def sub_aly(aly):
        limit = random()
        ualy = Tekoaly4(0)
        for n in aly.__neurons:
            if random() < limit:
                ualy.__neurons.append(Neuron.copy(n))
        return ualy


def luo_alyt():
    alyt = []
    for i in range(9):
        alyt.append(Tekoaly4())
    treenaa_alyt(alyt)


def lataa_alyt():
    alyt = []
    for i in range(9):
        alyt.append(Tekoaly4.load('Tekoaly' + str(i) + '.txt'))
    treenaa_alyt(alyt)


def treenaa_alyt(alyt):
    for polvi in range(1000):
        pisteet = []
        for p in range(len(alyt)):
            pisteet.append([p, 0])
        for i in range(len(alyt) - 1):
            aly1 = alyt[i]
            for j in range(i + 1, len(alyt)):
                aly2 = alyt[j]
                p1, p2 = pelaa_vastakkain(aly1, aly2)
                pisteet[i][1] += p1
                pisteet[j][1] += p2
        print("Sukupolvi", polvi, "Pisteet", pisteet)
        uudista_alyt(alyt, pisteet)
    for i in range(len(alyt)):
        alyt[i].save('Tekoaly' + str(i) + '.txt')


def pelaa_vastakkain(aly1, aly2):
    p1 = 0
    p2 = 0
    v = pelaa_ilman_piirtoa(aly1.choose, aly2.choose)
    if v == 0:
        aly1.mark_neurons(True)
        aly2.mark_neurons(False)
        p1 += 2
    elif v == -1:
        aly1.mark_neurons(True)
        aly2.mark_neurons(True)
        p1 += 1
        p2 += 1
    else:
        aly1.mark_neurons(False)
        aly2.mark_neurons(True)
        p2 += 2

    v = pelaa_ilman_piirtoa(aly2.choose, aly1.choose)
    if v == 0:
        aly2.mark_neurons(True)
        aly1.mark_neurons(False)
        p2 += 2
    elif v == -1:
        aly1.mark_neurons(True)
        aly2.mark_neurons(True)
        p1 += 1
        p2 += 1
    else:
        aly2.mark_neurons(False)
        aly1.mark_neurons(True)
        p1 += 2
    return p1, p2


def uudista_alyt(alyt, pisteet):
    pisteet.sort(key=lambda x: x[1], reverse=True)
    print(pisteet)
    alyt2 = alyt[:]
    for i in range(3):
        alyt[i] = alyt2[pisteet[i][0]]

    alyt[3] = Tekoaly4.copy(alyt[0])
    alyt[3].mutate()
    alyt[4] = Tekoaly4.copy(alyt[1])
    alyt[4].mutate()
    alyt[5] = Tekoaly4.copy(alyt[2])
    alyt[5].mutate()
    alyt[6] = Tekoaly4.combine(alyt[0], alyt[1])
    alyt[7] = Tekoaly4.combine(alyt[0], alyt[2])
    alyt[8] = Tekoaly4.combine(alyt[1], alyt[2])


def testaa_aly(tiedostonimi):
    aly = Tekoaly4.load(tiedostonimi)
    pisteet = 0
    for i in range(50):
        v = pelaa_ilman_piirtoa(aly.choose, syote_satunnaisesti)
        if v != 1:
            pisteet += 1
        v = pelaa_ilman_piirtoa(syote_satunnaisesti, aly.choose)
        if v != 0:
            pisteet += 1
    print("Aly vs satunnainen", pisteet)

    pisteet = 0
    for i in range(50):
        v = pelaa_ilman_piirtoa(aly.choose, syote_algoritmilta)
        if v != 1:
            pisteet += 1
        v = pelaa_ilman_piirtoa(syote_algoritmilta, aly.choose)
        if v != 0:
            pisteet += 1
    print("Aly vs algoritmi", pisteet)


def testaa_kaikki(raja=8):
    for i in range(raja + 1):
        tnimi = 'Tekoaly' + str(i) + ".txt"
        print(tnimi)
        testaa_aly(tnimi)


def luo_yksi():
    aly = Tekoaly4()
    treenaa_yksi(aly)


def treenaa_yksi(aly):
    for polvi in range(20):
        temp = Tekoaly4.copy(aly)
        while True:
            v = pelaa_ilman_piirtoa(aly.choose, temp.choose)


def treenaa2(alyt):
    for polvi in range(100):
        print("Sukupolvi", polvi)
        for i in range(len(alyt)):
            aly1 = alyt[i]
            for j in range(i + 1, len(alyt)):
                aly2 = alyt[j]
                v = pelaa_ilman_piirtoa(aly1.choose, aly2.choose)
                aly1.mark_neurons(v != 1)
                aly2.mark_neurons(v != 0)
                v = pelaa_ilman_piirtoa(aly2.choose, aly1.choose)
                aly1.mark_neurons(v != 0)
                aly2.mark_neurons(v != 1)
        for aly in alyt:
            aly.mutate(print_info=True)
    for i in range(len(alyt)):
        alyt[i].save('Tekoaly' + str(i) + ".txt")


def treenaa3(aly : Tekoaly4):
    for polvi in range(100):
        voitot = 0
        for i in range(5):
            v = pelaa_ilman_piirtoa(aly.choose, syote_satunnaisesti)
            if v != 1:
                voitot += 1
                aly.mark_neurons(True)
            else:
                aly.mark_neurons(False)
            v = pelaa_ilman_piirtoa(syote_satunnaisesti, aly.choose)
            if v != 0:
                voitot += 1
                aly.mark_neurons(True)
            else:
                aly.mark_neurons(False)
        print("Sukupolvi", polvi, "Voitot:", voitot)
        aly.mutate(print_info=True)
    aly.save('Tekoaly0.txt')


def treenaa_yhta():
    alyt = [Tekoaly4.load('Tekoaly.txt')]
    for polvi in range(10000):
        temp = Tekoaly4.copy(alyt[-1])
        temp.add_neuron()
        while True:
            parempi = etsi_parempi(alyt, temp)
            if parempi is not None:
                break
        print("Sukupolvi", polvi, "Aly kehittyi!")
        parempi.print_size()
        alyt.append(parempi)
        if (polvi + 1) % 10 == 0:
            print("Tallennetaan")
            alyt[-1].save('Tekoaly.txt')



def etsi_parempi(alyt, temp):
    if voittaa_kaikki(alyt, temp):
        return temp
    temp.change_last()
    subi = Tekoaly4.sub_aly(alyt[randint(0, len(alyt) - 1)])
    if voittaa_kaikki(alyt, subi):
        return subi
    return None


def voittaa_kaikki(alyt, aly):
    for aly2 in alyt:
        v = pelaa_ilman_piirtoa(aly.choose, aly2.choose)
        if v == 1:
            return False
        v = pelaa_ilman_piirtoa(aly2.choose, aly.choose)
        if v == 0:
            return False
    return True


#treenaa_yhta()
#treenaa3(Tekoaly4.load('Tekoaly0.txt'))
#luo_alyt()
#lataa_alyt()
testaa_aly('Tekoaly.txt')
#testaa_kaikki()