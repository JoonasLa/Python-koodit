__author__ = 'Joonas'

from Tekoaly import Tekoaly
from Tekoaly import Matrix
from random import randint
from Bantumi import pelaa_ilman_piirtoa, pelaa_peli, syote_algoritmilta, \
    syote_pelaajalta, syote_satunnaisesti


def luo_uudet_alyt():
    alyt = []
    for i in range(5):
        a = Tekoaly([14, 80, 6])
        a.random()
        alyt.append(a)

    for i in range(5):
        a = Tekoaly([14, 20, 20, 6])
        a.random()
        alyt.append(a)

    treenaa_tekoalyja(alyt)


def lataa_alyt():
    alyt = []
    for i in range(10):
        loadname = "Tekoaly" + str(i) + ".txt"
        alyt.append(Tekoaly.load(loadname))
    treenaa_tekoalyja(alyt)


def treenaa_tekoalyja(alyt):

    for polvi in range(50):
        pisteet = pelatkaa_vastakkain(alyt)
        print("Sukupolvi:", polvi, "Pisteet:", pisteet)
        uudista_alyt(pisteet)

        # minp = pisteet[0][1]
        # mini = 0
        # for i, p in enumerate(pisteet):
        #     if p[1] < minp:
        #         minp = p[1]
        #         mini = i
        # parantele_alya(alyt, mini, minp)

    for i in range(len(alyt)):
        nimi = "Tekoaly" + str(i) + ".txt"
        alyt[i].save(nimi)


def parantele_alya(alyt, kohde, alkup):
    temp = Tekoaly.copy_aly(alyt[kohde])
    vp = alkup
    turhat_yrit = 0
    while True:
        alyt[kohde].mutate()
        pisteet = testaa_muita_vastaan(alyt, kohde, tavoite=vp)
        if pisteet == 36:
            print(alyt[kohde], "voitti kaikki")
            return
        elif pisteet == vp:
            turhat_yrit += 1
        elif pisteet > vp:
            temp.copy(alyt[kohde])
            vp = pisteet
            print(alyt[kohde], "paransi pisteitään", vp)
            turhat_yrit = 0
        else:
            turhat_yrit += 1

            alyt[kohde].copy(temp)
            if turhat_yrit > 100:
                print("Kestää liian kauan, jatketaan seuraavaan")
                return


def testaa_muita_vastaan(alyt, kohde, tavoite=0):
    aly1 = alyt[kohde]
    pisteet = 0
    pisteet_jaljella = len(alyt) * 4 - 4
    for aly2 in alyt[:kohde] + alyt[kohde+1:]:
        voittaja = pelaa_ilman_piirtoa(aly1.choose, aly2.choose)
        if voittaja == 0:
            pisteet += 2
        elif voittaja == -1:
            pisteet += 1
        voittaja = pelaa_ilman_piirtoa(aly2.choose, aly1.choose)
        if voittaja == 1:
            pisteet += 2
        elif voittaja == -1:
            pisteet += 1
        pisteet_jaljella -= 4
        if pisteet_jaljella + pisteet < tavoite:
            return pisteet
    return pisteet


def pelatkaa_vastakkain(alyt):
    pisteet = []

    for aly in alyt:
        pisteet.append([aly, 0])
    for i in range(len(alyt)):
        pisteet[i][1] += testaa_muita_vastaan(alyt, i)

        voittaja = pelaa_ilman_piirtoa(alyt[i].choose, syote_algoritmilta)
        if voittaja == 0:
            pisteet[i][1] += 10
        elif voittaja == -1:
            pisteet[i][1] += 5

        voittaja = pelaa_ilman_piirtoa(syote_algoritmilta, alyt[i].choose)
        if voittaja == 1:
            pisteet[i][1] += 10
        elif voittaja == -1:
            pisteet[i][1] += 5

    return pisteet


def treenaa_algoritmilla(alyt):
    pisteet = []

    for aly in alyt:
        pisteet.append([aly, 0])

    for i in range(len(alyt)):
        for j in range(5):
            voittaja = pelaa_ilman_piirtoa(alyt[i].choose, syote_algoritmilta)
            if voittaja == 0:
                pisteet[i][1] += 2
            elif voittaja == -1:
                pisteet[i][1] += 1

            voittaja = pelaa_ilman_piirtoa(syote_algoritmilta, alyt[i].choose)
            if voittaja == 1:
                pisteet[i][1] += 2
            elif voittaja == -1:
                pisteet[i][1] += 1

    return pisteet


def uudista_alyt(pisteet):
    pisteet.sort(key=lambda x: x[1], reverse=True)
    alyt = []
    for i in pisteet:
        alyt.append(i[0])
    alyt[5].copy(alyt[0])
    alyt[5].mutate()
    alyt[5].copy(alyt[1])
    alyt[6].mutate()
    alyt[7].copy(alyt[2])
    alyt[7].mutate()
    alyt[8] = Tekoaly.random_aly()
    alyt[9] = Tekoaly.random_aly()
    return alyt


def pelaa_tekoalyn_kanssa():
    aly = Tekoaly.load("Tekoaly3.txt")
    pelaa_peli(aly.choose, syote_pelaajalta)


def testaa_aly(tiedostonimi):
    aly = Tekoaly.load(tiedostonimi)

    voitot = testipelit(aly.choose, syote_satunnaisesti)
    print("Aly1 vs satunnainen", voitot)

    voitot = testipelit(aly.choose, syote_algoritmilta)
    print("Aly1 vs algoritmi", voitot)

def testaa_eka():
    aly = Tekoaly.load("Ekapelaaja.txt")
    voitot = 0
    for i in range(100):
        v = pelaa_ilman_piirtoa(aly.choose, syote_satunnaisesti)
        if v == 0:
            voitot += 1
    print("Ekaäly vs satunnainen", voitot)
    voitot = 0
    for i in range(100):
        v = pelaa_ilman_piirtoa(aly.choose, syote_algoritmilta)
        if v == 0:
            voitot += 1
    print("Ekaäly vs algoritmi", voitot)

def testaa_toka():
    aly = Tekoaly.load("Tokapelaaja.txt")
    voitot = 0
    for i in range(100):
        v = pelaa_ilman_piirtoa(syote_satunnaisesti, aly.choose)
        if v == 1:
            voitot += 1
    print("Tokaäly vs satunnainen", voitot)
    voitot = 0
    for i in range(100):
        v = pelaa_ilman_piirtoa(syote_algoritmilta, aly.choose)
        if v == 1:
            voitot += 1
    print("Tokaäly vs algoritmi", voitot)

def testipelit(testi, vastustaja):
    voitot = 0
    for i in range(50):
        voittaja = pelaa_ilman_piirtoa(testi, vastustaja)
        if voittaja == 0:
            voitot += 1
        voittaja = pelaa_ilman_piirtoa(vastustaja, testi)
        if voittaja == 1:
            voitot += 1
    return voitot

def luo_yksi_aly():
    aly = Tekoaly([14,10, 10, 6])
    aly.random()
    #treenaa_yksin(aly)
    pelaa_satunnaista_vastaan(aly)

def lataa_yksi_aly():
    #aly = Tekoaly.load("Yksin_treenattu.txt")
    #treenaa_yksin(aly)
    aly = Tekoaly.load("Satunnaisen_teurastaja.txt")
    pelaa_satunnaista_vastaan(aly)

def treenaa_yksin(aly):
    fiksut = [aly]
    for polvi in range(10):
        mutaatiot = []
        while len(mutaatiot) < 50:
            mut = Tekoaly.copy_aly(fiksut[-1])
            mutaatiot.append(mut)
            for m in mutaatiot:
                m.mutate()
                if parjaa_fiksuille(fiksut, m):
                    print("Sukupolvi:", polvi, "Mutaatio on voittanut!")
                    fiksut.append(Tekoaly.copy_aly(m))
                    break
            else:
                continue
            break
        print("Siirrytään seuraavaan")

    fiksut[-1].save("Yksin_treenattu.txt")

def parjaa_fiksuille(fiksut, m):
    for f in fiksut:
        if pelaa_ilman_piirtoa(f.choose, m.choose) == 0 or \
                    pelaa_ilman_piirtoa(m.choose, f.choose) == 1:
            return False
    return True

def pelaa_satunnaista_vastaan(aly):
    maxvoitot = pelit_satunnaista_vastaan(aly)
    print("Vanha äly voitti", maxvoitot)
    mutaatiot = []
    while maxvoitot < 10:
        mutaatiot.append(Tekoaly.copy_aly(aly))
        mutaatiot.append(Tekoaly.random_aly())
        for m in mutaatiot:
            m.mutate()
            v = pelit_satunnaista_vastaan(m)
            if v > maxvoitot:
                print("Mutaatio paransi tulosta:", v)
                maxvoitot = v
                aly.copy(m)
                mutaatiot = []
    aly.save("Satunnaisen_teurastaja.txt")

def pelit_satunnaista_vastaan(m):
    voitot = 0
    for i in range(5):
        if pelaa_ilman_piirtoa(m.choose, syote_algoritmilta) == 0:
            voitot += 1
        if pelaa_ilman_piirtoa(syote_algoritmilta, m.choose) == 1:
            voitot += 1
    return voitot

def aly_molemmille():
    aly1 = Tekoaly([14, 20, 20, 6])
    aly2 = Tekoaly([14, 20, 20, 6])
    aly1.random()
    aly2.random()
    treenaa_molempia(aly1, aly2)

def lataa_aly_molemmille():
    aly1 = Tekoaly.load("Ekapelaaja.txt")
    aly2 = Tekoaly.load("Tokapelaaja.txt")
    treenaa_molempia(aly1, aly2)

def treenaa_molempia(aly1, aly2):
    alkv = pelaa_ilman_piirtoa(aly1.choose, aly2.choose)
    v = alkv
    for polvi in range(10000):
        while True:
            if v == 0:
                aly2.mutate()
            else:
                aly1.mutate()
            v1 = pelaa_ilman_piirtoa(aly1.choose, syote_algoritmilta) == 0
            v2 = pelaa_ilman_piirtoa(syote_algoritmilta, aly2.choose) == 1
            if v1 and not v2:
                v = 0
            elif v2 and not v1:
                v = 1
            else:
                v = pelaa_ilman_piirtoa(aly1.choose, aly2.choose)
            if v != alkv:
                print("Sukupolvi", polvi, "Kehitystä tapahtuu")
                alkv = v
                break
        if polvi % 100 == 0:
            print("Tallennetaan")
            aly1.save("Ekapelaaja.txt")
            aly2.save("Tokapelaaja.txt")

    aly1.save("Ekapelaaja.txt")
    aly2.save("Tokapelaaja.txt")


#lataa_aly_molemmille()

#lataa_alyt()

#aly_molemmille()

#luo_uudet_alyt()

#luo_yksi_aly()

#lataa_yksi_aly()

