__author__ = 'Joonas'

from Tekoaly import Matrix
from random import randint

class Bantumi:
    def __init__(self):
        self.__pelilauta = [[4, 4, 4, 4, 4, 4, 0], [4, 4, 4, 4, 4, 4, 0]]
        self.__vuoro = 0

    def voi_siirtaa(self, paikka):
        return self.__pelilauta[self.__vuoro][paikka] != 0

    def siirra(self, paikka):
        papuja = self.__pelilauta[self.__vuoro][paikka]
        if papuja == 0:
            raise Exception("NOLLA PAPUA, paikka", paikka)
        self.__pelilauta[self.__vuoro][paikka] = 0
        puoli = self.__vuoro
        while papuja > 0:
            paikka += 1
            if paikka == 7:
                paikka = 0
                puoli = (puoli + 1) % 2
            self.__pelilauta[puoli][paikka] += 1
            papuja -= 1
        if puoli == self.__vuoro:
            if paikka == 6:
                self.__vuoro = (self.__vuoro + 1) % 2
            elif self.__pelilauta[puoli][paikka] == 1:
                self.__pelilauta[puoli][paikka] = 0
                self.__pelilauta[puoli][6] += \
                    1 + self.__pelilauta[(puoli + 1) % 2][5-paikka]
                self.__pelilauta[(puoli + 1) % 2][5-paikka] = 0
        self.__vuoro = (self.__vuoro + 1) % 2
        return self.__vuoro, self.peli_paattynyt()

    def peli_paattynyt(self):
        for i in self.__pelilauta[self.__vuoro][:-1]:
            if i != 0:
                return False
        return True

    def piirra_lauta(self):
        print("\n    ", end="")
        for i in range(5, -1, -1):
            luku = str(self.__pelilauta[1][i])
            print(luku, " "*(4 - len(luku)), end="")
        luku = str(self.__pelilauta[1][6])
        print("\n" + luku, " "*(32 - len(luku)), self.__pelilauta[0][6],
              end="\n    ")
        for i in range(0, 6):
            luku = str(self.__pelilauta[0][i])
            print(luku, " "*(4 - len(luku)), end="")
        print("\n Pelaaja:", self.__vuoro + 1)

    def laske_pisteet(self):
        p1 = 0
        p2 = 0
        for i in range(7):
            p1 += self.__pelilauta[0][i]
            p2 += self.__pelilauta[1][i]
        return p1, p2

    def palauta_tilanne(self):
        return self.__pelilauta, self.__vuoro

    def palauta_alylle(self, pelaaja):
        tila = [i/10 for i in self.__pelilauta[pelaaja]] + \
               [i/10 for i in self.__pelilauta[(pelaaja + 1) % 2]]
        return Matrix.array2matrix(tila)

    def palauta_list(self, pelaaja):
        tila = [i/10 for i in self.__pelilauta[pelaaja]] + \
               [i/10 for i in self.__pelilauta[(pelaaja + 1) % 2]]
        return tila

    def palauta_tuple(self, pelaaja):
        tila = [i for i in self.__pelilauta[pelaaja]] + \
               [i for i in self.__pelilauta[(pelaaja + 1) % 2]]
        return tuple(tila)


def syote_pelaajalta(peli):
    while True:
        try:
            luku = int(input())
            if 0 <= luku < 6:
                if peli.voi_siirtaa(luku):
                    return luku
                else:
                    print("Valitse paikka jossa on papuja")
            else:
                print("Anna luku väliltä 0-5")
        except ValueError:
            print("Anna luku")


def syote_algoritmilta(peli):
    lauta, vuoro = peli.palauta_tilanne()

    varasmax = 0
    varasi = None
    for i in range(5):
        if not peli.voi_siirtaa(i):
            continue
        kohde = lauta[vuoro][i] + i
        if kohde < 6 and lauta[vuoro][kohde] == 0:
            toinenpuoli = lauta[(vuoro + 1) % 2][5-kohde]
            if toinenpuoli > varasmax:
                varasmax = toinenpuoli
                varasi = i
    if varasi is not None:
        return varasi
    for i in range(6):
        if lauta[vuoro][5 - i] == i + 1:
            return 5 - i
    return syote_satunnaisesti(peli)


def syote_satunnaisesti(peli):
    lista = [0, 1, 2, 3, 4, 5]
    while True:
        luku = randint(0, len(lista) - 1)
        if peli.voi_siirtaa(lista[luku]):
            return lista[luku]
        lista.pop(luku)


def syote_eka_mahdollinen(peli):
    for i in range(6):
        if peli.voi_siirtaa(i):
            return i


def syote_vika_mahdollinen(peli):
    for i in range(5, -1, -1):
        if peli.voi_siirtaa(i):
            return i


def pelaa_peli(pelaaja1, pelaaja2):
    peli = Bantumi()
    syote = [pelaaja1, pelaaja2]
    vuoro = 0
    peli_paattynyt = False

    peli.piirra_lauta()
    while not peli_paattynyt:
        paikka = syote[vuoro](peli)
        print("Siirto paikasta", paikka)
        vuoro, peli_paattynyt = peli.siirra(paikka)
        peli.piirra_lauta()

    print("Peli on päättynyt")
    p1, p2 = peli.laske_pisteet()
    print("Pisteet: p1", p1, "p2", p2)
    if p1 > p2:
        print("Onnittelut pelaaja 1")
    elif p1 < p2:
        print("Onnittelut pelaaja 2")
    else:
        print("Uskomatonta! Tasapeli")


def pelaa_ilman_piirtoa(pelaaja1, pelaaja2):
    peli = Bantumi()
    syote = [pelaaja1, pelaaja2]
    vuoro = 0
    peli_paattynyt = False

    while not peli_paattynyt:
        paikka = syote[vuoro](peli)
        vuoro, peli_paattynyt = peli.siirra(paikka)

    p1, p2 = peli.laske_pisteet()
    if p1 > p2:
        return 0
    elif p1 < p2:
        return 1
    else:
        return -1
