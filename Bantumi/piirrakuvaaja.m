% Piirt�� kuvaajan fotonin energiasta sek� elektronin liike-energiasta
el = 1.602176462e-19;
E = 59.5e3 * el
h = 6.62606876e-34;
c = 299792458;
me = 9.10938188e-31;
theta = [0:0.01:2*pi];
Et = E./(1+E/(me * c^2)*(1 - cos(theta)))./el * 1e-3;
degs = theta*360/(2*pi);
figure(1)
plot(degs, Et, 'k')
xticks(0:45:360)
yticks(48:1:60)
axis([0 360 48 60])
xlabel('Siroamiskulma (deg)')
ylabel('Fotonin energia (keV)')

figure(2)
KE = E/el/1e3 - Et
plot(degs, KE, 'k')
axis([0 360 0 12])
xticks(0:45:360)
yticks(0:1:12)
xlabel('Siroamiskulma (deg)')
ylabel('Elektronin liike-energia (keV)')
