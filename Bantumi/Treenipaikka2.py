__author__ = 'Joonas'

from Vektorialy import Vektorialy
from Bantumi import pelaa_ilman_piirtoa, syote_satunnaisesti, syote_algoritmilta

def luo_uudet():
    aly1 = Vektorialy()
    aly2 = Vektorialy()
    aly1.save('Vektorialy1.txt')
    aly2.save('Vektorialy2.txt')

def treenaa_alya():
    aly1 = Vektorialy.load('Vektorialy1.txt')
    aly2 = Vektorialy.load('Vektorialy2.txt')
    for polvi in range(1):
        print("Sukupolvi", polvi)
        v = pelaa_ilman_piirtoa(aly1.choose, aly2.choose)
        if v == 0:
            aly1.save_on_won()
            aly2.save_on_lost()
        elif v == 1:
            aly1.save_on_lost()
            aly2.save_on_won()
        print("Voittaja", v+1)
        v = pelaa_ilman_piirtoa(aly2.choose, aly1.choose)
        if v == 1:
            aly1.save_on_won()
            aly2.save_on_lost()
        elif v == 0:
            aly1.save_on_lost()
            aly2.save_on_won()
        print("Voittaja", 2-v)

    aly2.save('Vektorialy2.txt')
    aly1.save('Vektorialy1.txt')


def testaa_aly(tiedostonimi):
    aly = Vektorialy.load(tiedostonimi)
    voitot = 0
    for i in range(50):
        v = pelaa_ilman_piirtoa(aly.choose, syote_satunnaisesti)
        if v == 0:
            voitot += 1
        v = pelaa_ilman_piirtoa(syote_satunnaisesti, aly.choose)
        if v == 1:
            voitot += 1
    print("Aly vs Satunnainen", voitot)

    voitot = 0
    for i in range(50):
        v = pelaa_ilman_piirtoa(aly.choose, syote_algoritmilta)
        if v == 0:
            voitot += 1
        v = pelaa_ilman_piirtoa(syote_algoritmilta, aly.choose)
        if v == 1:
            voitot += 1
    print("Aly vs Algoritmi", voitot)

#luo_uudet()
treenaa_alya()
#testaa_aly('Vektorialy1.txt')

