__author__ = 'Joonas'

from random import random, randint
from Bantumi import Bantumi, syote_algoritmilta, syote_satunnaisesti, \
    pelaa_ilman_piirtoa

class Neuron():

    def __init__(self, rand=True):
        self.__input = []
        self.__output = None
        self.__bias = 0
        self.__times_activated = 0
        self.__activation_value = 0
        self.__total_times_activated = 0
        if rand:
            self.randomize()

    def randomize(self):
        limit = random()
        for i in range(14):
            r = random()
            if r < limit:
                w = (i, (r / limit) * 2 - 1)
                self.__input.append(w)
        if len(self.__input) == 0:
            w = (randint(0, 13), random() * 2 - 1)
            self.__input.append(w)

        limit = random()
        for i in range(6):
            r = random()
            if r < limit:
                w = (i, (r / limit) * 2 - 1)
                self.__output.append(w)
        if len(self.__output) == 0:
            w = (randint(0, 5), random() * 2 - 1)
            self.__output.append(w)

        self.__bias = random() * 2 - 1

    def set_neuron(self, line):
        inputs, outputs, bias = line.split("|")
        inputs = inputs.split(" ")
        for l in inputs:
            try:
                i, w = l.split(":")
                self.__input.append((int(i), float(w)))
            except ValueError:
                continue
        outputs = outputs.split(" ")
        for l in outputs:
            i, w = l.split(":")
            self.__output.append((int(i), float(w)))
        self.__bias = float(bias)

    def choose(self, inp, output):
        s = 0
        for i, w in self.__input:
            s += inp[i] * w
        if s > self.__bias:
            for i, w in self.__output:
                output[i] += w
            self.__times_activated += 1

    def no_activation(self):
        return self.__total_times_activated == 0

    def mark_value(self, bool_won):
        if bool_won:
            self.__activation_value += self.__times_activated
        else:
            self.__activation_value -= self.__times_activated
        self.__total_times_activated += self.__times_activated
        self.__times_activated = 0

    def return_value(self):
        return self.__activation_value

    def reset_activations(self):
        self.__activation_value = 0
        self.__times_activated = 0
        self.__total_times_activated = 0

    def __str__(self):
        line = ""
        for i, w in self.__input:
            line += str(i) + ":" + str(w) + " "
        line = line[:-1] + "|"
        for i, w in self.__output:
            line += str(i) + ":" + str(w) + " "
        line = line[:-1] + "|" + str(self.__bias)
        return line

    @staticmethod
    def copy(another):
        neuron = Neuron(rand=False)
        neuron.__output = deepcopy(another.__output)
        neuron.__input = deepcopy(another.__input)
        neuron.__bias = another.__bias
        return neuron

class Tekoaly5():
