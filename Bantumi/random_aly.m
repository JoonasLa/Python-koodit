function [aly] = random_aly(layers)
aly = {}
for l = 2:size(layers)
    weight = rand(layers(l - 1), layers(l)) * 2 - 1;
    bias = rand(layers(l), 1)

end