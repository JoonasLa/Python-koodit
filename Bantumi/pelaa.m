function [voittaja] = pelaa(aly1, aly2)

lauta = [4 4 4 4 4 4 0 4 4 4 4 4 4 0];
vuoro = 0;
while peliJatkuu(lauta, vuoro)
    if vuoro == 0
        siirto = aly1(lauta, vuoro);
    else
        siirto = aly2(lauta, vuoro);
    end
    
    papuja = lauta(siirto);
    lauta(siirto) = 0;
    for k = 1:papuja
        paikka = mod(siirto + k - 1, 14) + 1;
        lauta(paikka) = lauta(paikka) + 1;
    end
    vuoro = mod(vuoro + 1, 2);
    if paikka - 7 * vuoro == 7
        vuoro = mod(vuoro + 1, 2);
    elseif lauta(paikka) == 1 && fix(paikka, 7) == vuoro
        vastap = mod(vuoro + 1, 2) * 7 + 7 - mod(paikka, 7);
        lauta(vuoro * 7 + 7) = lauta(vuoro * 7 + 7) + lauta(vastap) + 1;
        lauta(vastap) = 0 ;
    end
end

p1 = lauta(7);
p2 = lauta(14);

if p1 > p2
    voittaja = 0;
elseif p2 > p1
    voittaja = 1;
else
    voittaja = -1;
end
end

function [jatkuu] = peliJatkuu(lauta, vuoro)
for k = 1:6
    if lauta(k + 7 * vuoro) ~= 0
        jatkuu = true;
        return
    end
jatkuu = false;
end
end