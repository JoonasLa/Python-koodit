__author__ = 'Joonas'

from random import randint, random
from Bantumi import syote_satunnaisesti
inf = float('inf')

class Vektorialy():
    def __init__(self):
        self.__vectors = {}
        first_case = (4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4, 0)
        self.__vectors[first_case] = [0] * 6
        self.__cases2save = {}

    def choose(self, game):
        board, player = game.palauta_tilanne()
        board = game.palauta_tuple(player)
        opportunities = self.find_closest(board)[:]
        s = 0
        for i, val in enumerate(opportunities):
            try:
                s += 2 ** val
            except OverflowError:
                self.__cases2save[board] = i
                print(val)
                return i
        r = random() * s
        for i, val in enumerate(opportunities):
            r -= 2 ** val
            if r <= 0 and game.voi_siirtaa(i):
                self.__cases2save[board] = i
                return i
        syote = syote_satunnaisesti(game)
        self.__cases2save[board] = syote
        return syote

    def find_closest(self, board):
        if board not in self.__vectors:
            self.__vectors[board] = [0] * 6
            for i in range(6):
                if board[i] == 0:
                    self.__vectors[board][i] = -inf
        return self.__vectors[board]


    @staticmethod
    def calc_distance(board1, board2):
        d = 0
        for b1, b2 in zip(board1, board2):
            d += abs(b2 - b1)
        return d

    def save_on_won(self):
        for v in self.__cases2save:
            c = self.__cases2save[v]
            if v not in self.__vectors:
                self.__vectors[v] = [0] * 6
            self.__vectors[v][c] += 1
        self.__cases2save = {}

    def save_on_lost(self):
        for v in self.__cases2save:
            c = self.__cases2save[v]
            if v not in self.__vectors:
                self.__vectors[v] = [0] * 6
            self.__vectors[v][c] -= 1
        self.__cases2save = {}

    def save(self, filename):
        file = open(filename, 'w')
        line = ""
        for v in self.__vectors:
            if line != "":
                file.write(line + "\n")
            line = ""
            for vv in v[:-1]:
                line += str(vv) + " "
            line += str(v[-1]) + ":"
            for vv in self.__vectors[v][:-1]:
                line += str(vv) + " "
            line += str(self.__vectors[v][-1])
        file.write(line)
        file.close()

    @staticmethod
    def load(loadname):
        aly = Vektorialy()
        aly.__vectors = {}
        file = open(loadname, 'r')
        for line in file:
            line = line.strip()
            vector, op = line.split(":")
            vector = vector.split(" ")
            for i in range(14):
                vector[i] = int(vector[i])
            vector = tuple(vector)
            op = op.split(" ")
            for i in range(6):
                if op[i] != "-inf":
                    op[i] = int(op[i])
                else:
                    op[i] = -inf
            aly.__vectors[vector] = op
        file.close()
        return aly


