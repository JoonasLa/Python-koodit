__author__ = 'Joonas'

from random import randint, random, shuffle
from Bantumi import Bantumi

MIN_AFFECTING = 1
MIN_NEURONS = 20

class Neuron():
    def __init__(self, prev_layer=None, next_layer=None):
        self.__previous_layer = prev_layer
        self.__next_layer = next_layer
        self.__input = []
        self.__output = []
        self.__bias = 0
        self.__value = 0
        self.__zero = False
        self.__one = False

    def randomize(self):
        a = randint(MIN_AFFECTING, len(self.__previous_layer))
        jump = len(self.__previous_layer) / a
        st = randint(0, int(jump))
        for j in range(a):
            r = randint(-1, 1)
            if r != 0:
                self.__input.append([int(st), r])
            st += jump

        a = randint(MIN_AFFECTING, len(self.__next_layer))
        jump = len(self.__previous_layer) / a
        st = randint(0, int(jump))
        for j in range(a):
            r = randint(-1, 1)
            if r != 0:
                self.__input.append([int(st), r])
            st += jump
        self.__bias = randint(0, 10)

    def calc_value(self, normalize):
        s = 0
        for i, weight in self.__input:
            s += self.__previous_layer[i].__value * weight
        if normalize:
            if s < self.__bias:
                self.__value = 0
                self.__zero = True
            else:
                self.__value = 1
                self.__one = True
        else:
            self.__value = s


    def set_value(self, value):
        self.__value = value

    def check_activitations(self):
        if self.__one and not self.__zero:
            return 1
        if self.__zero and not self.__one:
            return -1
        return 0

    def mutate(self, bias_change=0):
        if bias_change != 0:
            self.__bias -= randint(1, 5) * bias_change
            return
        for i in range(len(self.__input)):
            self.__input[i] += randint(-1, 1)





class Tekoaly3():
    def __init__(self, neurons=):
        self.__layer = []
        l1 = []
        for i in range(6):
            l1.append(Neuron())
        self.__layer.append(l1)
