__author__ = 'Joonas'

import math
from random import random
from time import time


def satunnainen_hidas():
    luvut = [0] * 11
    for i in range(1):
        kerta = 0
        raja = random()
        for k in range(10):
            if random() < raja:
                kerta += 1
        luvut[kerta] += 1
    return luvut

def satunnainen_nopea():
    luvut = [0] * 11
    for i in range(1):
        kerta = 0
        raja = random()
        siemen = random()
        liikkuja = siemen
        for k in range(10):
            if liikkuja < raja:
                kerta += 1
            liikkuja += siemen
            liikkuja -= int(liikkuja)
        luvut[kerta] += 1
    return luvut

def maarita_kesto(funktio, toistot):
    alku = time()
    for i in range(toistot):
        funktio()
    loppu = time()
    print("Funktio:", funktio.__name__, "Aika:", loppu - alku, "Keskimääräinen funktion suoritusaika:",
          (loppu - alku) / toistot)

def tyhja():
    pass

def hidas_lista():
    lista = []
    for i in range(10000000):
        lista.append(0)

def nopea_lista():
    lista = [math.pi]*10000000

def none_lista():
    lista = [None]*10000000

def nopea_lista2():
    lista = [None]*10000000
    for i in range(10000000):
        lista[i] = 0

def sijoita_1():
    x = 1
    y = 2
    return

def sijoita_rand():
    x = random()
    y = random()
    return

maarita_kesto(sijoita_rand, 1000000)
maarita_kesto(sijoita_1, 1000000)