__author__ = 'Joonas'

import random
import time

SUURIN_NUMERO = 10
LUKUJEN_MAARA = 2000


def main():
    aika0 = time.time()
    lista = luo_lista()
    aika1 = time.time()

    lista2 = jarjesta1(lista)

    aika2 = time.time()
    print(lista2)
    print(aika2-aika1)




def luo_lista():
    lista = []
    i = 0
    while i < LUKUJEN_MAARA:
        lista.append(random.randint(0, SUURIN_NUMERO))
        i += 1
    return lista

def jarjesta1(lista):
    eka = {}
    for luku in lista:
        if str(luku) in eka:
            eka[str(luku)] += 1
        else:
            eka[str(luku)] = 1
    palautus = []
    palautusnimi = []
    for luku in eka:
        sijoitettu = False
        for i in range(len(palautus)):
            if eka[luku] >= palautus[i]:
                palautus.insert(i, eka[luku])
                palautusnimi.insert(i, luku)
                sijoitettu = True
                break
        if not sijoitettu:
            palautus.append(eka[luku])
            palautusnimi.append(luku)

    return palautus

def jarjesta2(lista):
    palautusnimi = []
    palautus = []
    for luku in lista:
        loyty = False
        for i in range(len(palautusnimi)):
            if str(luku) == str(palautusnimi[i]):
                if i != 0 and palautus[i-1] < palautus[i] + 1:
                    vali = palautusnimi[i]
                    vali2 = palautus[i]
                    palautus[i] = palautus[i-1]
                    palautus[i-1] = vali2 + 1
                    palautusnimi[i] = palautusnimi[i-1]
                    palautusnimi[i-1] = vali
                else:
                    palautus[i] += 1
                loyty = True
                break
        if not loyty:
            palautus.append(1)
            palautusnimi.append(str(luku))

    return palautus

def jarjesta3(lista):
    lapikertaa = 0
    jarjestyksessa = False
    while not jarjestyksessa:
        edellinen = lista[0]
        jarjestyksessa = True
        for i in range(1, len(lista)):
            if lista[i] > edellinen:
                vali = lista[i]
                lista[i] = lista[i-1]
                lista[i-1] = vali
                jarjestyksessa = False
            edellinen = lista[i]
        lapikertaa += 1
    print("Lista käytiin läpi", lapikertaa)
    return lista


def tarkista(lista):
    summa = 0
    edellinen = LUKUJEN_MAARA
    for luku in lista:
        if luku > edellinen:
            print("LISTA EI JÄRJESTYKSESSÄ")
            return
        edellinen = luku
        summa += luku
    print("LISTA JÄRJESTYKSESSÄ")
    if summa == LUKUJEN_MAARA:
        print("LUKUJA OIKEA MÄÄRÄ")
    else:
        print("LUKUJA VÄÄRÄ MÄÄRÄ")


main()