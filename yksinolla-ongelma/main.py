__author__ = 'Joonas'

from collections import deque


class IsoLuku:
    def __init__(self, luku=0, jarjestelma=10):
        self.__luku = luku
        self.__jarjestelma = jarjestelma
        self.__lj = []
        x = luku
        while x > 0:
            self.__lj.append(x % jarjestelma)
            x = x // jarjestelma
        self.__pituus = len(self.__lj)
        self.__lisaysmahdollisuus = 0
        for indeksi, arvo in enumerate(self.__lj):
            if arvo == 0:
                self.__lisaysmahdollisuus += jarjestelma**indeksi

    def lisaa_ykkonen(self):
        for i in range(self.__pituus):
            self.__lj[i] = 0
        self.__lj.append(1)
        self.__pituus += 1
        self.__luku = self.__jarjestelma ** (self.__pituus - 1)
        self.__lisaysmahdollisuus = (self.__jarjestelma**(self.__pituus - 1) - 1)\
                             //(self.__jarjestelma - 1)

    def suurenna_lukua(self, raja):
        while raja - self.__luku > self.__lisaysmahdollisuus:
            self.lisaa_ykkonen()
        erotus = raja - self.__luku
        lm2 = self.__lisaysmahdollisuus
        for i in range(self.__pituus - 2, -1, -1):
            if self.__lj[i] == 0:
                lisays = self.__jarjestelma ** i
                if lisays < erotus or lm2 - lisays < erotus:
                    self.__lj[i] = 1
                    self.__luku += lisays
                    self.__lisaysmahdollisuus -= lisays
                    erotus -= lisays
                    if erotus <= 0:
                        break
                lm2 -= lisays

    def isompi(self, toinen):
        if self.__luku < toinen.__luku:
            return toinen
        return self

    def pienempi(self, toinen):
        if self.__luku > toinen.__luku:
            return toinen
        return self

    def tulosta_luku(self):
        print("Luku:", self.__luku, "pituus", self.__pituus, "Järjestelmä:", self.__jarjestelma,
              "Luku järjestelmässä:", end=" ")
        for i in range(self.__pituus - 1, -1, -1):
            print(self.__lj[i], end="")
        print("")

    def palauta_pituus(self):
        return self.__pituus

    def palauta_arvo(self):
        return self.__luku


def testaa_luku(luku, aste):
    for a in range(3, aste):
        luku2 = luku
        while luku2 > 0:
            if luku2 % a > 1:
                return False
            luku2 = luku2 // a
    return True


def muodosta_n_jarjestelmaesitys(luku, n):
    lukul = []
    while luku > 0:
        lukul.append(luku % n)
        luku = luku // n
    lukul.reverse()
    print("Järjestelmä:", n, end=" : ")
    for l in lukul:
        print(l, end="")
    print(" ", end="\n")


def etsi_tyhmasti():
    aste = 4
    lista = deque([1])
    while len(lista) < 100000:
        for a in range(2):
            luku = lista[0]*aste + a
            lista.append(luku)
            if testaa_luku(luku, aste):
                print("Jiippii jne, luku:", lista[-1])
                for i in range(2, aste + 1):
                    muodosta_n_jarjestelmaesitys(luku, i)
        lista.popleft()
    print("Valmista, viimeinen luku", lista[-1])


def kokeile_eri_lukuja():
    uusi = ""
    while uusi == "":
        luku = int(input("Luku: "))
        j = int(input("Max j: "))
        for i in range(3, j + 1):
            muodosta_n_jarjestelmaesitys(luku, i)
        uusi = input("Uudestaan?")


def suurenna_lukuja():

    tarkasteltavat = [5, 6]
    luvut = []
    for i in tarkasteltavat:
        luvut.append(IsoLuku(luku=i+1, jarjestelma=i))

    kertaa = 0
    while luvut[0].palauta_pituus() < 1000:
        kertaa += 1
        isoin = luvut[0]
        pienin = luvut[0]
        for i in range(1, len(luvut)):
            isoin = isoin.isompi(luvut[i])
            pienin = pienin.pienempi(luvut[i])
        if isoin == pienin:
            print("JIPPII, luvut löytyivät")
            for luku in luvut:
                luku.tulosta_luku()
            isoin.suurenna_lukua(isoin.palauta_arvo() + 1)
        pienin.suurenna_lukua(isoin.palauta_arvo())
    print("Eipä löytynyt :-(")
    print("Looppi käyty läpi", kertaa, "kertaa")
    print("Vikat luvut:")
    for luku in luvut:
        luku.tulosta_luku()


class TosiIsoLuku:
    def __init__(self, jarjestelma):
        self.__jarjestelma = jarjestelma
        self.__maksimi = 1
        self.__minimi = 1
        self.__nmaara = 1

    def lisaa_luku(self):
        self.__nmaara += 1
        self.__minimi *= self.__jarjestelma
        self.__maksimi += self.__minimi

    def palauta_maksimi(self):
        return self.__maksimi

    def palauta_minimi(self):
        return self.__minimi

    def palauta_vali(self):
        return (self.__minimi, self.__maksimi)

    def tuo_samalle_tasolle(self, toinen):
        if self.__maksimi > toinen.__minimi:
            luku = self.__maksimi - self.__minimi
            while luku >= toinen.__minimi:
                print("Jatketaan pienemmällä luvulla :?D")
                self.__maksimi = luku
                self.__minimi = self.__minimi // self.__jarjestelma
                self.__nmaara -= 1
                luku = self.__maksimi - self.__minimi
        elif self.__maksimi < toinen.__minimi:
            while self.__maksimi < toinen.__minimi:
                self.lisaa_luku()

    def voi_lisata_lisaa(self, toinen):
        luku = self.__minimi * self.__jarjestelma
        return luku <= toinen.__maksimi

    @staticmethod
    def luvut_risteaa(eka, toka):
        return max(eka.__minimi, toka.__minimi) <= min(eka.__maksimi, toka.__maksimi)


    def __str__(self):
        p = str(self.__minimi) + " - " + str(self.__maksimi)
        return p


def valit_risteaa(vali1, vali2):
    return max(vali1[0], vali2[0]) <= min(vali1[1], vali2[1])

def vali_risteaa_lista(vali, lista):
    for v in lista:
        if valit_risteaa(vali, v):
            return True
    return False

def valit_kuntoon(luku1, luku2, tarkasta):
    valit = [deque(), deque()]
    valit[0].append(luku1.palauta_vali())
    valit[1].append(luku2.palauta_vali())
    minimit = [luku1.palauta_minimi(), luku2.palauta_minimi()]
    while True:
        for i in range(2):
            t = len(valit[i])
            for k in range(t):
                pass




def kokeile_eksponentteja():
    tarkasta = [3, 4]
    luvut = []
    for i in tarkasta:
        luvut.append(TosiIsoLuku(i))

    for nmaara in range(2, 10):
        luvut[0].lisaa_luku()
        luvut[1].tuo_samalle_tasolle(luvut[0])
        while True:
            if TosiIsoLuku.luvut_risteaa(luvut[0], luvut[1]):
                valit_kuntoon(luvut[0], luvut[1], tarkasta)
            if luvut[1].voi_lisata_lisaa(luvut[0]):
                luvut[1].lisaa_luku()
            else:
                break

def kokeile_eksponentteja2():
    tarkasta = [5, 6]
    maksimit = [1, 1]
    minimit = [1, 1]
    muuttuvat = [1, 1]
    valit1 = deque()
    valit2 = deque()
    for i in range(2, 100000):
        minimit[0] *= tarkasta[0]
        maksimit[0] += minimit[0]
        vali = (minimit[0], maksimit[0])
        valit1.append(vali)
        if maksimit[1] - minimit[1] >= minimit[0]:
            print("SE ON MAHDOLLISTA")
            maksimit[1] -= minimit[1]
            minimit[1] = minimit[1] // tarkasta[1]
        while maksimit[1] < minimit[0]:
            minimit[1] *= tarkasta [1]
            maksimit[1] += minimit[1]
            if maksimit[1] < minimit[0]:
                print("PITÄ Ä TEHDÄ UUDESTAAN")
        vali = (minimit[1], maksimit[1])
        valit2.append(vali)
        muuttuvat = [minimit[0], minimit[1]]
        while True:
            muuttuvat[0] = muuttuvat[0] // tarkasta[0]
            muuttuvat[1] = muuttuvat[1] // tarkasta[1]
            t = len(valit1)
            if muuttuvat[0] != 0:
                for k in range(t):
                    vali = (valit1[0][0], valit1[0][1] - muuttuvat[0])
                    if vali_risteaa_lista(vali, valit2):
                        valit1.append(vali)
                    vali = (valit1[0][0] + muuttuvat[0], valit1[0][1])
                    if vali_risteaa_lista(vali, valit2):
                        valit1.append(vali)
                    valit1.popleft()
            if len(valit1) == 0:
                break

            t = len(valit2)
            if muuttuvat[1] != 0:
                for k in range(t):
                    vali = (valit2[0][0], valit2[0][1] - muuttuvat[1])
                    if vali_risteaa_lista(vali, valit1):
                        valit2.append(vali)
                    vali = (valit2[0][0] + muuttuvat[1], valit2[0][1])
                    if vali_risteaa_lista(vali, valit1):
                        valit2.append(vali)
                    valit2.popleft()
            if len(valit2) == 0:
                break
            if muuttuvat[0] == 0 and muuttuvat[1] == 0:
                for i in valit1:
                    print("Löytyi! Luku:", i[0])
                    for j in tarkasta:
                        muodosta_n_jarjestelmaesitys(i[0], j)
                break
    print(minimit[0], "\n", minimit[1])


def main():
    kokeile_eksponentteja2()

main()
