__author__ = 'Joonas'


def main():
    tiedosto = open("ajat", "r")
    taulukko = {}
    pvm = "0"
    for jono in tiedosto:
        rivi = jono.rstrip()
        if len(rivi) == 0:
            continue
        if len(rivi.split("-")) == 1:
            pvm = rivi
            if rivi not in taulukko:
                tunnit = [" "]*48
                taulukko[rivi] = tunnit
            continue
        alku = int(float(rivi.split("-")[0])*2.0)
        loppu = int(float(rivi.split("-")[1])*2.0)
        if alku < loppu:
            for i in range(alku, loppu):
                taulukko[pvm][i] = "x"
        else:
            for i in range(alku, 48):
                taulukko[pvm][i] = "x"
            tunnit = [" "]*48
            upaiva = str((int(pvm))%30 + 1)
            taulukko[upaiva] = tunnit
            for i in range(0, loppu):
                taulukko[upaiva][i] = "x"

    kuva = ""
    for i in range(18, 18+3*7):
        for j in taulukko[str((i-1) % 30 + 1)]:
            kuva += j + "\t"
        kuva += "\n"
    print(kuva)

    xmaara = []
    for h in range(48):
        maara = 0
        for p in range(18, 18+3*7):
            if taulukko[str((p-1)%30+1)][h] == "x":
                maara += 1
        xmaara.append(maara)
    rivi = ""
    for luku in xmaara:
        suhde = int(luku/21*100)
        rivi += str(suhde) + "\t"
    print(rivi)

    summa = 0
    for luku in xmaara:
        summa += luku
    print("Nukuin yhteensä", summa/2, "tuntia, eli", summa/(2*21), "tuntia vuorokaudessa")

    pisin = 0
    valveilla = 0
    for p in range(18, 18+21):
        for h in range(48):
            if taulukko[str((p-1)%30 + 1)][h] == " ":
                valveilla += 1
            else:
                if valveilla > pisin:
                    pisin = valveilla
                valveilla = 0
    print("Pisin aika valveilla oli", pisin/2, "tuntia")

main()