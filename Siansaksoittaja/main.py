__author__ = 'Joonas'

from random import randint

EDELTAVAT_KIRJAIMET = 7
SANOJA = 10

def main():
    tiedosto = open('kotus_sanat.txt', 'r')
    sanat = []
    for sana in tiedosto:
        sanat.append(sana.strip())
    tiedosto.close()

    kirj_yhd = {}
    aloitukset = dict(summa=0)
    for s in sanat:
        if len(s) < EDELTAVAT_KIRJAIMET:
            continue
        alku = s[:EDELTAVAT_KIRJAIMET]
        if alku in aloitukset:
            aloitukset[alku] += 1
        else:
            aloitukset[alku] = 1
        aloitukset['summa'] += 1

        for i in range(len(s) - EDELTAVAT_KIRJAIMET):
            yhd = s[i:i+EDELTAVAT_KIRJAIMET]
            seuraava = s[i+EDELTAVAT_KIRJAIMET]
            if yhd in kirj_yhd:
                if seuraava in kirj_yhd[yhd]:
                    kirj_yhd[yhd][seuraava] += 1
                else:
                    kirj_yhd[yhd][seuraava] = 1
                kirj_yhd[yhd]['summa'] += 1
            else:
                kirj_yhd[yhd] = {}
                kirj_yhd[yhd][seuraava] = 1
                kirj_yhd[yhd]['summa'] = 1
        yhd = s[-EDELTAVAT_KIRJAIMET:]
        if yhd in kirj_yhd:
            if ' ' in kirj_yhd[yhd]:
                kirj_yhd[yhd][' '] += 1
            else:
                kirj_yhd[yhd][seuraava] = 1
        else:
            kirj_yhd[yhd] = {}
            kirj_yhd[yhd][' '] = 1
            kirj_yhd[yhd]['summa'] = 1

    print('SAnat luettu')
    uudet_sanat = []
    while len(uudet_sanat) < SANOJA:
        r = randint(1, aloitukset['summa'])
        for alku in aloitukset:
            if alku == 'summa':
                continue
            r -= aloitukset[alku]
            if r <= 0:
                uusisana = alku
                break

        while True:
            loppu = uusisana[-EDELTAVAT_KIRJAIMET:]
            if loppu in kirj_yhd:
                r = randint(1, kirj_yhd[loppu]['summa'])
                for kirjain in kirj_yhd[loppu]:
                    if kirjain == 'summa':
                        continue
                    r -= kirj_yhd[loppu][kirjain]
                    if r <= 0:
                        uusisana += kirjain
                        break
            else:
                uusisana += ' '

            if uusisana[-1] == ' ':
                uudet_sanat.append(uusisana[:-1])
                break


    for sana in uudet_sanat:
        print(sana, end=' ')




main()