__author__ = 'Joonas'

import random
VOKAALIT = ["a", "e", "i", "o", "u", "y", "ä", "ö"]
def main():
    teksti = "Onpas tämäkin turha ohjelma"
    sanat = teksti.split(" ")
    tulos = []
    for sana in sanat:
        if len(sana) < 4:
            tulos.append(sana)
            continue
        vokaalit = []
        konsonantit = []
        for i in sana[1:-1]:
            if i in VOKAALIT:
                vokaalit.append(i)
            else:
                konsonantit.append(i)
        random.shuffle(vokaalit)
        random.shuffle(konsonantit)
        usana = sana[0]

        if sana[0] in VOKAALIT:
            for i in vokaalit:
                usana += i
            for i in konsonantit:
                usana += i
        else:
            for i in konsonantit:
                usana += i
            for i in vokaalit:
                usana += i
        usana += sana[-1]
        tulos.append(usana)
    uteksti = ""
    for i in tulos:
        uteksti += i + " "
    print(uteksti)

main()