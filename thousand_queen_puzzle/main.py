__author__ = 'Joonas'
import time

KOKO = 23


def main():
    aika1 = time.time()
    ruudukko = []
    for i in range(KOKO):
        rivi = [0]*KOKO
        ruudukko.append(rivi)
    ratkaisu = ratkaise_ongelma(ruudukko, 0)
    aika2 = time.time()
    print("Aikaa kului:", aika2 - aika1)
    tulosta_ruudukko(ratkaisu)

def ratkaise_ongelma(ruudukko, x):
    if x == KOKO - 1:
        for y in range(KOKO):
            if ruudukko[y][x] == 0:
                return [(x, y)]
        return None
    mahikset = []
    for y in range(KOKO):
        if ruudukko[y][x] != 0:
            continue
        minpaikat = laske_minimi_paikat(ruudukko, x, y)
        if minpaikat > 0:
            mahikset.append((y, minpaikat))
    if len(mahikset) == 0:
        return None
    mahikset.sort(key=lambda x: x[1], reverse=True)
    for y, paikat in mahikset:
        tee_siirto(ruudukko, x, y)
        seuraava = ratkaise_ongelma(ruudukko, x + 1)
        if seuraava is None:
            peru_siirto(ruudukko, x, y)
            continue
        else:
            seuraava.append((x, y))
            return seuraava

def laske_minimi_paikat(ruudukko, x, y):
    minimi = KOKO
    for xx in range(x + 1, KOKO):
        luku = 0
        for yy in range(KOKO):
            if ruudukko[yy][xx] == 0 and not samalla_linjalla(x, y, xx, yy):
                luku += 1
        if luku < minimi:
            minimi = luku
    for yy in range(KOKO):
        if yy == y:
            continue
        luku = 0
        for xx in range(0, x + 1):
            if ruudukko[yy][xx] == -1:
                break
        else:   #Ei löytynyt valmiisti sijoitettua kuningatarta
            for xx in range(x + 1, KOKO):
                if ruudukko[yy][xx] == 0 and not samalla_linjalla(x, y, xx, yy):
                    luku += 1
            if luku < minimi:
                minimi = luku
    return minimi

def samalla_linjalla(x1, y1, x2, y2):
    if y1 == y2 or x1 == x2 or x1 + y1 == x2 + y2 or x1 - y1 == x2 - y2:
        return True
    return False

def tee_siirto(ruudukko, x, y):
    ruudukko[y][x] = -1
    for i in range(1, KOKO - x):
        ruudukko[y][x + i] += 1
        if y - i >= 0:
            ruudukko[y - i][x + i] += 1
        if y + i < KOKO:
            ruudukko[y + i][x + i] += 1

def peru_siirto(ruudukko, x, y):
    ruudukko[y][x] = 0
    for i in range(1, KOKO - x):
        ruudukko[y][x + i] -= 1
        if y - i >= 0:
            ruudukko[y - i][x + i] -= 1
        if y + i < KOKO:
            ruudukko[y + i][x + i] -= 1

def tulosta_ruudukko(kuningattaret):
    ruutu = []
    for j in range(KOKO):
        ruutu.append(["."]*KOKO)
    for x, y in kuningattaret:
        ruutu[y][x] = "X"
    tuloste = ""
    for i in range(KOKO):
        for j in range(KOKO):
            tuloste += ruutu[i][j]
        tuloste += "\n"
    print(tuloste)

def tarkista_ratkaisu(kuningattaret):
    for i, p in enumerate(kuningattaret):
        x, y = p[0], p[1]
        for xx, yy in kuningattaret[i+1:]:
            if samalla_linjalla(x, y, xx, yy):
                print("SAMALLA LINJALLA! (", x, y, ") (", xx, yy, ")")
                break
        else:
            continue
        break
    print("JEEP KAIKKI KUNNOSSA BOIIIII :-D")

main()