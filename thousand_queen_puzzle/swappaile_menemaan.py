__author__ = 'Joonas'

KOKO = 4



def main():
    ruudukko = list(range(KOKO))
    swappaile_ite(ruudukko)

def swappaile_ite(ruudukko):
    while True:
        tulosta_ruudukko(ruudukko)
        syote = input("Siirrot: ")
        try:
            if syote == "":
                return
            numerot = syote.split(" ")
            x1 = int(numerot[0]) - 1
            x2 = int(numerot[1]) - 1
            ruudukko[x1], ruudukko[x2] = ruudukko[x2], ruudukko[x1]
            if tarkasta_valmius(ruudukko):
                print("Valmis!")
                tulosta_ruudukko(ruudukko)
                return
        except:
            print("Virheellinen syöte!")

def tarkasta_valmius(ruudukko):
    for x1, y1 in enumerate(ruudukko[:-1]):
        for x2, y2 in enumerate(ruudukko[x1 + 1:]):
            if sama_linja(x1, y1, x2 + x1 + 1, y2):
                return False
    return True

def sama_linja(x1, y1, x2, y2):
    return x1 + y1 == x2 + y2 or x1 - y1 == x2 - y2

def ratkaise_swappailemalla(ruudukko):
    for i in range(len(ruudukko)):

def tulosta_ruudukko(ruudukko):
    ruutu = [[". "] * KOKO for i in range(KOKO)]
    for x, y in enumerate(ruudukko):
        ruutu[y][x] = "X "
    for rivi in ruutu:
        print("".join(rivi))


main()